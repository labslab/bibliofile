o arquivo dump.sql possui o dump da base de dados. 
Alternativamente há o script prepdb.sql que criará uma base de dados vazia chamada 
'bibliofile', o usuário 'hibernate' e dará as permissões de modificação da base para o usuário 'hibernate'.

Faça o deploy do war  no tomcat e o hibernate crirá as tabelas.

O tomcat deve estar configurado com um timeout de inicialização grande o suficiente para dar tempo da base de 
dados ser criada.

para criar as permissões básicas acesse a url localhost:8080/bibliofile/populate/roles
para criar um usuário com permissões de administrador com senha 1234 localhost:8080/bibliofile/populate/admin
para criar um usuário com permissões de bibliotecário com senha 1234 localhost:8080/bibliofile/populate/bibliotecario

para criar um usuário comum acesse a página de criação do usuário http://localhost:8080/bibliofile/usuario/cadastro.

para efetuar um empréstimo logue como bibliotecário, selecione o livro e clique no link sobre o título do livro. selecione o nome do usuário e 
envie.


