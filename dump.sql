-- MySQL dump 10.15  Distrib 10.0.26-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: bibliofile
-- ------------------------------------------------------
-- Server version	10.0.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Author`
--

DROP TABLE IF EXISTS `Author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Author` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Author`
--

LOCK TABLES `Author` WRITE;
/*!40000 ALTER TABLE `Author` DISABLE KEYS */;
INSERT INTO `Author` VALUES (1,'Brian Ward'),(2,'Nicholas S. Williams'),(3,'Bill Burke'),(4,'Friedrich Nietzsche'),(5,'Plato'),(6,'Kathy Sierra & Bert Bates'),(7,'Michael Morrison ');
/*!40000 ALTER TABLE `Author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Code`
--

DROP TABLE IF EXISTS `Code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Code`
--

LOCK TABLES `Code` WRITE;
/*!40000 ALTER TABLE `Code` DISABLE KEYS */;
INSERT INTO `Code` VALUES (1,'9781593275679'),(2,'1118656466'),(3,'144936134X'),(4,'0140441182'),(5,'0465069347'),(6,'8576081733'),(7,'8576082136');
/*!40000 ALTER TABLE `Code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Form`
--

DROP TABLE IF EXISTS `Form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Form` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Form`
--

LOCK TABLES `Form` WRITE;
/*!40000 ALTER TABLE `Form` DISABLE KEYS */;
INSERT INTO `Form` VALUES (1,'BA'),(2,'BC');
/*!40000 ALTER TABLE `Form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item`
--

DROP TABLE IF EXISTS `Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item` (
  `DTYPE` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `issn` varchar(255) DEFAULT NULL,
  `issnl` varchar(255) DEFAULT NULL,
  `rawcoverage` varchar(255) DEFAULT NULL,
  `ed` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `originalLang` varchar(255) DEFAULT NULL,
  `year` date DEFAULT NULL,
  `createdBy_id` bigint(20) DEFAULT NULL,
  `lastModifiedBy_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3ewd6ywj9tpdqjehhdvwm3pey` (`createdBy_id`),
  KEY `FK7dluia9ffwwg1exmfqat2mdu2` (`lastModifiedBy_id`),
  CONSTRAINT `FK3ewd6ywj9tpdqjehhdvwm3pey` FOREIGN KEY (`createdBy_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FK7dluia9ffwwg1exmfqat2mdu2` FOREIGN KEY (`lastModifiedBy_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item`
--

LOCK TABLES `Item` WRITE;
/*!40000 ALTER TABLE `Item` DISABLE KEYS */;
INSERT INTO `Item` VALUES ('ISBN',1,'2016-10-14 17:12:53','2016-10-14 17:12:53','','AVAILABLE','How Linux Works.',NULL,NULL,NULL,'','eng','',NULL,5,5),('ISBN',2,'2016-10-14 17:14:50','2016-10-14 17:23:35','Indianapolis, Ind.','BORROWED','Professional Java for web applications : featuring WebSockets, Spring Framework, JPA Hibernate, and Spring Security',NULL,NULL,NULL,'','eng','',NULL,5,5),('ISBN',3,'2016-10-14 17:16:15','2016-10-14 17:16:15','Beijing, CN','AVAILABLE','RESTful Java with JAX-RS 2.0',NULL,NULL,NULL,'Second edition.','eng','',NULL,5,5),('ISBN',4,'2016-10-14 17:20:42','2016-10-14 17:20:42','Baltimore','AVAILABLE','Thus spoke Zarathustra; a book for everyone and no one.',NULL,NULL,NULL,'[Nachdr.].','eng','',NULL,5,5),('ISBN',5,'2016-10-14 17:33:54','2016-10-14 17:33:54','[New York]','AVAILABLE','The Republic of Plato',NULL,NULL,NULL,'2nd ed.','eng','',NULL,5,5),('ISBN',6,'2016-10-14 17:53:32','2016-10-14 17:53:32','Rio de Janeiro','AVAILABLE','Use a cabeça! Java',NULL,NULL,NULL,'2.ed.','por','',NULL,5,5),('ISBN',7,'2016-10-14 17:54:52','2016-10-14 17:54:52','Rio de Janeiro (RJ)','AVAILABLE','Use a cabeça : JavaScript',NULL,NULL,NULL,'','por','',NULL,5,5),('ISBN',8,'2016-10-14 17:55:50','2016-10-14 17:55:50','Rio de Janeiro (RJ)','AVAILABLE','Use a cabeça : JavaScript',NULL,NULL,NULL,'','por','',NULL,5,5);
/*!40000 ALTER TABLE `Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_Author`
--

DROP TABLE IF EXISTS `Item_Author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_Author` (
  `ISBN_id` bigint(20) NOT NULL,
  `author_id` bigint(20) NOT NULL,
  KEY `FKkntw4x4w3v92yay5amfjyoxy8` (`author_id`),
  KEY `FKcbybaqg3kfakwa4xjvwvdlxwb` (`ISBN_id`),
  CONSTRAINT `FKcbybaqg3kfakwa4xjvwvdlxwb` FOREIGN KEY (`ISBN_id`) REFERENCES `Item` (`id`),
  CONSTRAINT `FKkntw4x4w3v92yay5amfjyoxy8` FOREIGN KEY (`author_id`) REFERENCES `Author` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_Author`
--

LOCK TABLES `Item_Author` WRITE;
/*!40000 ALTER TABLE `Item_Author` DISABLE KEYS */;
INSERT INTO `Item_Author` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,7);
/*!40000 ALTER TABLE `Item_Author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_Code`
--

DROP TABLE IF EXISTS `Item_Code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_Code` (
  `ISBN_id` bigint(20) NOT NULL,
  `code_id` bigint(20) NOT NULL,
  KEY `FKdndrns1ojf44ynobx55umhfya` (`code_id`),
  KEY `FKsgrloqy7s0a5ef9qer2wh6rie` (`ISBN_id`),
  CONSTRAINT `FKdndrns1ojf44ynobx55umhfya` FOREIGN KEY (`code_id`) REFERENCES `Code` (`id`),
  CONSTRAINT `FKsgrloqy7s0a5ef9qer2wh6rie` FOREIGN KEY (`ISBN_id`) REFERENCES `Item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_Code`
--

LOCK TABLES `Item_Code` WRITE;
/*!40000 ALTER TABLE `Item_Code` DISABLE KEYS */;
INSERT INTO `Item_Code` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,7);
/*!40000 ALTER TABLE `Item_Code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_Form`
--

DROP TABLE IF EXISTS `Item_Form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_Form` (
  `Item_id` bigint(20) NOT NULL,
  `form_id` bigint(20) NOT NULL,
  KEY `FKc2p80is25htxhbqlmffkpdbl4` (`form_id`),
  KEY `FKjjcnoyiijmmpmssbg7h44u278` (`Item_id`),
  CONSTRAINT `FKc2p80is25htxhbqlmffkpdbl4` FOREIGN KEY (`form_id`) REFERENCES `Form` (`id`),
  CONSTRAINT `FKjjcnoyiijmmpmssbg7h44u278` FOREIGN KEY (`Item_id`) REFERENCES `Item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_Form`
--

LOCK TABLES `Item_Form` WRITE;
/*!40000 ALTER TABLE `Item_Form` DISABLE KEYS */;
INSERT INTO `Item_Form` VALUES (1,1),(2,2),(3,2),(4,2),(5,2),(6,1),(7,1),(8,1);
/*!40000 ALTER TABLE `Item_Form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_Oclcnum`
--

DROP TABLE IF EXISTS `Item_Oclcnum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_Oclcnum` (
  `Item_id` bigint(20) NOT NULL,
  `oclcnum_id` bigint(20) NOT NULL,
  KEY `FKoef04ix60no36iysrdp11yh7p` (`oclcnum_id`),
  KEY `FK7874nksmqcboupv7wwu0gvhor` (`Item_id`),
  CONSTRAINT `FK7874nksmqcboupv7wwu0gvhor` FOREIGN KEY (`Item_id`) REFERENCES `Item` (`id`),
  CONSTRAINT `FKoef04ix60no36iysrdp11yh7p` FOREIGN KEY (`oclcnum_id`) REFERENCES `Oclcnum` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_Oclcnum`
--

LOCK TABLES `Item_Oclcnum` WRITE;
/*!40000 ALTER TABLE `Item_Oclcnum` DISABLE KEYS */;
INSERT INTO `Item_Oclcnum` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,7);
/*!40000 ALTER TABLE `Item_Oclcnum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_PeerReview`
--

DROP TABLE IF EXISTS `Item_PeerReview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_PeerReview` (
  `ISSN_id` bigint(20) NOT NULL,
  `peerReview_id` bigint(20) NOT NULL,
  KEY `FKfhxfsguiw2yjtcirkjscha9xn` (`peerReview_id`),
  KEY `FKhgs4l39kb3raurb7u0416wguf` (`ISSN_id`),
  CONSTRAINT `FKfhxfsguiw2yjtcirkjscha9xn` FOREIGN KEY (`peerReview_id`) REFERENCES `PeerReview` (`id`),
  CONSTRAINT `FKhgs4l39kb3raurb7u0416wguf` FOREIGN KEY (`ISSN_id`) REFERENCES `Item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_PeerReview`
--

LOCK TABLES `Item_PeerReview` WRITE;
/*!40000 ALTER TABLE `Item_PeerReview` DISABLE KEYS */;
/*!40000 ALTER TABLE `Item_PeerReview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_Publisher`
--

DROP TABLE IF EXISTS `Item_Publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_Publisher` (
  `Item_id` bigint(20) NOT NULL,
  `publisher_id` bigint(20) NOT NULL,
  KEY `FK9ued0g5bpt0ytxsswtvl3hvtb` (`publisher_id`),
  KEY `FKhonpdbhcwttpa17cegq56gvbn` (`Item_id`),
  CONSTRAINT `FK9ued0g5bpt0ytxsswtvl3hvtb` FOREIGN KEY (`publisher_id`) REFERENCES `Publisher` (`id`),
  CONSTRAINT `FKhonpdbhcwttpa17cegq56gvbn` FOREIGN KEY (`Item_id`) REFERENCES `Item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_Publisher`
--

LOCK TABLES `Item_Publisher` WRITE;
/*!40000 ALTER TABLE `Item_Publisher` DISABLE KEYS */;
INSERT INTO `Item_Publisher` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,6),(8,6);
/*!40000 ALTER TABLE `Item_Publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Loan`
--

DROP TABLE IF EXISTS `Loan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Loan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `dueDate` date DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `createdBy_id` bigint(20) DEFAULT NULL,
  `lastModifiedBy_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `loans_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKajudmvqc1q0ok9g0dnv1r7n5u` (`createdBy_id`),
  KEY `FKtirqi8txvufrh46iaa5daah0y` (`lastModifiedBy_id`),
  KEY `FKr29blb8f6d01bt8b1u0rr7eel` (`item_id`),
  KEY `FK6fn0vvi3j31kqyski4rm6ohor` (`user_id`),
  KEY `FKjtbupor49npwuuj0saa8r3982` (`loans_id`),
  CONSTRAINT `FK6fn0vvi3j31kqyski4rm6ohor` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FKajudmvqc1q0ok9g0dnv1r7n5u` FOREIGN KEY (`createdBy_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FKjtbupor49npwuuj0saa8r3982` FOREIGN KEY (`loans_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FKr29blb8f6d01bt8b1u0rr7eel` FOREIGN KEY (`item_id`) REFERENCES `Item` (`id`),
  CONSTRAINT `FKtirqi8txvufrh46iaa5daah0y` FOREIGN KEY (`lastModifiedBy_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Loan`
--

LOCK TABLES `Loan` WRITE;
/*!40000 ALTER TABLE `Loan` DISABLE KEYS */;
INSERT INTO `Loan` VALUES (1,'2016-10-14 17:23:35','2016-10-14 17:25:47','2016-10-28',NULL,NULL,5,7,2,7,7);
/*!40000 ALTER TABLE `Loan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LoginTracker`
--

DROP TABLE IF EXISTS `LoginTracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LoginTracker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdBy_id` bigint(20) DEFAULT NULL,
  `lastModifiedBy_id` bigint(20) DEFAULT NULL,
  `loginTrack_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjptsqvfyyqwi58v342wwskdr5` (`createdBy_id`),
  KEY `FKg3h4e1l5g2emr8bma06mv7f4q` (`lastModifiedBy_id`),
  KEY `FKo1n8m6qfyhg1a806uf3fv82sw` (`loginTrack_id`),
  CONSTRAINT `FKg3h4e1l5g2emr8bma06mv7f4q` FOREIGN KEY (`lastModifiedBy_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FKjptsqvfyyqwi58v342wwskdr5` FOREIGN KEY (`createdBy_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FKo1n8m6qfyhg1a806uf3fv82sw` FOREIGN KEY (`loginTrack_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LoginTracker`
--

LOCK TABLES `LoginTracker` WRITE;
/*!40000 ALTER TABLE `LoginTracker` DISABLE KEYS */;
INSERT INTO `LoginTracker` VALUES (1,'2016-10-14 17:47:31','2016-10-14 17:47:31','LOGIN',5,5,5),(2,'2016-10-14 17:47:54','2016-10-14 17:47:54','LOGOUT',5,5,5),(3,'2016-10-14 17:48:52','2016-10-14 17:48:52','LOGIN',5,5,5);
/*!40000 ALTER TABLE `LoginTracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Oclcnum`
--

DROP TABLE IF EXISTS `Oclcnum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Oclcnum` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `num` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Oclcnum`
--

LOCK TABLES `Oclcnum` WRITE;
/*!40000 ALTER TABLE `Oclcnum` DISABLE KEYS */;
INSERT INTO `Oclcnum` VALUES (1,'866937365'),(2,'868079449'),(3,'858311780'),(4,'1099026'),(5,'24422598'),(6,'422875791'),(7,'817149060');
/*!40000 ALTER TABLE `Oclcnum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PeerReview`
--

DROP TABLE IF EXISTS `PeerReview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PeerReview` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PeerReview`
--

LOCK TABLES `PeerReview` WRITE;
/*!40000 ALTER TABLE `PeerReview` DISABLE KEYS */;
/*!40000 ALTER TABLE `PeerReview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Publisher`
--

DROP TABLE IF EXISTS `Publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Publisher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Publisher`
--

LOCK TABLES `Publisher` WRITE;
/*!40000 ALTER TABLE `Publisher` DISABLE KEYS */;
INSERT INTO `Publisher` VALUES (1,'No Starch Pr'),(2,'Wrox, A Wiley Brand'),(3,'O\'Reilly'),(4,'Penguin Books'),(5,'Basic Books'),(6,'Alta Books');
/*!40000 ALTER TABLE `Publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role`
--

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER'),(3,'ROLE_LIBRARIAN');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jreodf78a7pl5qidfh43axdfb` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (5,'$2a$10$GBqri2G01BEAMroJ06SIQOJsaunsK7BLqpdWViVL1GKAuEpa0.L2K','bibliotecario'),(6,'$2a$10$18PR9rGgHqAyo59BsZBTzOnvz.UkuSiMe23jShVJBK7vI2/WbUO7e','admin'),(7,'$2a$10$9aAJymQt5AwxXzAHMNVnT.qAvOMo.yRV1M4AN7K82..ITYcVrwJie','usuario');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_Role`
--

DROP TABLE IF EXISTS `User_Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_Role` (
  `User_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  KEY `FK7qnwwe579g9frolyprat52l4d` (`roles_id`),
  KEY `FKc52d1rv3ijbpu6lo2v3rej1tx` (`User_id`),
  CONSTRAINT `FK7qnwwe579g9frolyprat52l4d` FOREIGN KEY (`roles_id`) REFERENCES `Role` (`id`),
  CONSTRAINT `FKc52d1rv3ijbpu6lo2v3rej1tx` FOREIGN KEY (`User_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_Role`
--

LOCK TABLES `User_Role` WRITE;
/*!40000 ALTER TABLE `User_Role` DISABLE KEYS */;
INSERT INTO `User_Role` VALUES (5,2),(5,3),(6,1),(6,2),(7,2);
/*!40000 ALTER TABLE `User_Role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-14 18:12:16
