CREATE USER 'hibernate'@'localhost';
CREATE DATABASE bibliofile;
USE bibliofile;
GRANT ALL PRIVILEGES ON bibliofile.* TO 'hibernate'@'%';
FLUSH PRIVILEGES;
