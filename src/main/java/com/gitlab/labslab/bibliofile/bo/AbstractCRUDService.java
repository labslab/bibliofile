package com.gitlab.labslab.bibliofile.bo;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.labslab.bibliofile.dao.Dao;

@Service
@Transactional(readOnly = true)
public abstract class AbstractCRUDService<T> implements CRUDService<T> {

	@Autowired
	private Dao<T> dao;
	
	protected Dao<T> getDao() {
		return dao;
	}
	
	@Override
	public T loadById(long id) {
		return dao.findById(id);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void save(T entity) {
		dao.save(entity);
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(T entity) {
		dao.update(entity);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
	public void delete(T entity) {
		dao.remove(entity);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
	public void delete(long id) {
		dao.removeById(id);
	}

	@Override
	public Collection<T> list() {
		return dao.list();
	}

}
