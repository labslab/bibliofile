package com.gitlab.labslab.bibliofile.bo;

import java.util.Collection;

public interface CRUDService<T> {

	T loadById(long id);

	void save(T entity);

	void update(T entity);

	void delete(T entity);

	void delete(long id);

	Collection<T> list();
}
