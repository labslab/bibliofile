package com.gitlab.labslab.bibliofile.bo;

import java.util.Calendar;
import java.util.List;

import com.gitlab.labslab.bibliofile.model.ISBN;

public interface ISBNService extends ItemService<ISBN> {

	List<ISBN> searchByAuthor(String author);

	ISBN searchByISBN(String isbn);

	List<ISBN> searchByLanguage(String language);

	List<ISBN> searchByYear(Calendar year);

}
