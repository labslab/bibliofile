package com.gitlab.labslab.bibliofile.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.labslab.bibliofile.dao.AuthorDao;
import com.gitlab.labslab.bibliofile.dao.CodeDao;
import com.gitlab.labslab.bibliofile.dao.FormDao;
import com.gitlab.labslab.bibliofile.dao.ISBNDao;
import com.gitlab.labslab.bibliofile.dao.OclcnumDao;
import com.gitlab.labslab.bibliofile.dao.PublisherDao;
import com.gitlab.labslab.bibliofile.model.Author;
import com.gitlab.labslab.bibliofile.model.Code;
import com.gitlab.labslab.bibliofile.model.Form;
import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.IsbnDto;
import com.gitlab.labslab.bibliofile.model.ItemState;
import com.gitlab.labslab.bibliofile.model.Oclcnum;
import com.gitlab.labslab.bibliofile.model.Publisher;

@Service
public class ISBNServiceImpl extends AbstractCRUDService<ISBN> implements ISBNService {

	@Autowired
	private AuthorDao authorDao;

	@Autowired
	private PublisherDao publisherDao;

	@Autowired
	private OclcnumDao oclcnumDao;

	@Autowired
	private CodeDao codeDao;

	@Autowired
	private FormDao formDao;

	private static final String apiUrl = "http://xisbn.worldcat.org/webservices/xid/isbn/%s?method=getEditions&fl=*&format=json";

	
	@Override
	@Transactional
	public void save(ISBN entity) {
		List<Author> authors = new ArrayList<>();
		for (Author a : entity.getAuthor()) {
			Author author = authorDao.findByExactName(a.getName());
			if (author != null) {
				authors.add(author);
			} else {
				authors.add(a);
			}
		}
		entity.setAuthor(authors);

		List<Publisher> publishers = new ArrayList<>();
		for (Publisher p : entity.getPublisher()) {
			Publisher publisher = publisherDao.findByExactName(p.getName());
			if (publisher != null) {
				publishers.add(publisher);
			} else {
				publishers.add(p);
			}
		}
		entity.setPublisher(publishers);

		List<Oclcnum> oclcnums = new ArrayList<>();
		for (Oclcnum o : entity.getOclcnum()) {
			Oclcnum oclcnum = oclcnumDao.findByNum(o.getNum());
			if (oclcnum != null) {
				oclcnums.add(oclcnum);
			} else {
				oclcnums.add(o);
			}
		}
		entity.setOclcnum(oclcnums);

		List<Code> codes = new ArrayList<>();
		for (Code c : entity.getCode()) {
			Code code = codeDao.findByCode(c.getCode());
			if (code != null) {
				codes.add(code);
			} else {
				codes.add(c);

			}
		}
		entity.setCode(codes);
		
		List<Form> forms = new ArrayList<>();
		for (Form f : entity.getForm()) {
			Form form = formDao.findByName(f.getName());
			if (form != null) {
				forms.add(form);
			} else {
				forms.add(f);

			}
		}
		entity.setForm(forms);

		entity.setState(ItemState.AVAILABLE);
		super.getDao().save(entity);

	}

	@Override
	public List<ISBN> searchByTitle(String title) {
		return ((ISBNDao) super.getDao()).searchByTitle(title);
	}

	@Override
	public List<ISBN> searchByOclcnum(String oclcnum) {
		return ((ISBNDao) super.getDao()).searchByOclcnum(oclcnum);
	}

	@Override
	public List<ISBN> searchByPublisher(String publisher) {
		return ((ISBNDao) super.getDao()).searchByPublisher(publisher);
	}

	@Override
	public List<ISBN> searchByForm(String form) {
		return ((ISBNDao) super.getDao()).searchByForm(form);
	}

	@Override
	public List<ISBN> searchByCity(String city) {
		return ((ISBNDao) super.getDao()).searchByCity(city);
	}

	@Override
	public List<ISBN> searchByAuthor(String author) {
		return ((ISBNDao) super.getDao()).searchByAuthor(author);
	}

	@Override
	public ISBN searchByISBN(String isbn) {
		return ((ISBNDao) super.getDao()).searchByISBN(isbn);
	}

	@Override
	public List<ISBN> searchByLanguage(String language) {
		return searchByLanguage(language);
	}

	@Override
	public List<ISBN> searchByYear(Calendar year) {
		return searchByYear(year);
	}

	private ISBN fromJson(String jsonInput) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		IsbnDto isbndto = mapper.readValue(jsonInput, IsbnDto.class);
		if (isbndto.getStat().equals("ok")) {
			ISBN isbn = new ISBN();
			isbndto = isbndto.getDtos().get(0);
			List<Author> authors = new ArrayList<>();
			isbndto.getAuthor().forEach(a -> authors.add(new Author(a)));
			List<Publisher> publishers = new ArrayList<>();
			isbndto.getPublisher().forEach(p -> publishers.add(new Publisher(p)));
			List<Form> forms = new ArrayList<>();
			isbndto.getForm().forEach(f -> forms.add(new Form(f)));
			List<Code> codes = new ArrayList<>();
			isbndto.getCode().forEach(c -> codes.add(new Code(c)));
			List<Oclcnum> oclcs = new ArrayList<>();
			isbndto.getOclcnum().forEach(o -> oclcs.add(new Oclcnum(o)));
			isbn.setAuthor(authors);
			isbn.setCode(codes);
			isbn.setEd(isbndto.getEd());
			isbn.setPublisher(publishers);
			isbn.setForm(forms);
			isbn.setOclcnum(oclcs);
			isbn.setCity(isbndto.getCity());
			isbn.setLang(isbndto.getLang());
			isbn.setTitle(isbndto.getTitle());
			isbn.setYear(isbndto.getYear());

			return isbn;
		}
		return null;
	}

	@Override
	public ISBN acquire(String isbn) {
		ISBN book = null;
		try {
			String urlString = String.format(apiUrl, isbn);
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String nl;

			while ((nl = in.readLine()) != null) {
				sb.append(nl);
			}

			book = fromJson(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return book;
	}

}
