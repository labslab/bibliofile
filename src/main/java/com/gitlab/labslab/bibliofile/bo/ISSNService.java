package com.gitlab.labslab.bibliofile.bo;

import java.util.List;

import com.gitlab.labslab.bibliofile.model.ISSN;

public interface ISSNService extends ItemService<ISSN> {

	ISSN searchByIssnl(String issnl);

	ISSN searchByIssn(String issn);

	List<ISSN> searchByRawCoverage(String rawCoverage);
}
