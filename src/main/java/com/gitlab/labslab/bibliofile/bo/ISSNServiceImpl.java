package com.gitlab.labslab.bibliofile.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.labslab.bibliofile.dao.ISSNDao;
import com.gitlab.labslab.bibliofile.model.ISSN;

@Service
public class ISSNServiceImpl extends AbstractCRUDService<ISSN> implements ISSNService {


	private static final String apiUrl = "http://xissn.worldcat.org/webservices/xid/issn/"
			+ "%s?method=getEditions&fl=*&format=json";

	@Override
	public List<ISSN> searchByTitle(String title) {
		return ((ISSNDao) super.getDao()).searchByTitle(title);
	}

	@Override
	public List<ISSN> searchByOclcnum(String oclnum) {
		return ((ISSNDao) super.getDao()).searchByOclcnum(oclnum);
	}

	@Override
	public List<ISSN> searchByPublisher(String publisher) {
		return ((ISSNDao) super.getDao()).searchByPublisher(publisher);
	}

	@Override
	public List<ISSN> searchByForm(String form) {
		return ((ISSNDao) super.getDao()).searchByForm(form);
	}

	@Override
	public List<ISSN> searchByCity(String city) {
		return ((ISSNDao) super.getDao()).searchByCity(city);
	}

	@Override
	public ISSN searchByIssnl(String issnl) {
		return ((ISSNDao) super.getDao()).searchByIssnl(issnl);
	}

	@Override
	public ISSN searchByIssn(String issn) {
		return ((ISSNDao) super.getDao()).searchByIssn(issn);
	}

	@Override
	public List<ISSN> searchByRawCoverage(String rawCoverage) {
		return searchByRawCoverage(rawCoverage);
	}

//	private ISSN fromJson(String jsonInput) throws JsonParseException, JsonMappingException, IOException {
//		ObjectMapper mapper = new ObjectMapper();
//		JsonNode jo = mapper.readTree(jsonInput);
//
//		if (jo.get("stat").equals("ok")) {
//
//			JsonNode jar = jo.get("group").get(0);
//			String jsonString = jar.toString();
//			mapper = new ObjectMapper();
//			IssnDto issn = mapper.readValue(jsonString, IssnDto.class);
//			issn.setStat(jo.get("stat").asText());
//			
//			return issn.getDtos().get(0);
//		}
//		return null;
//	}

	@Override
	public ISSN acquire(String isbn) {
//		ISSN journal = null;
//		try {
//			URL url = new URL(String.format(apiUrl, isbn));
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//			connection.setRequestMethod("GET");
//			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//			StringBuilder sb = new StringBuilder();
//			String nl;
//
//			while ((nl = in.readLine()) != null) {
//				sb.append(nl);
//			}
//
//			journal = fromJson(sb.toString());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return journal;
		return null;
	}

}
