package com.gitlab.labslab.bibliofile.bo;

import java.util.List;

public interface ItemService<Item> extends CRUDService<Item> {

		
	List<Item> searchByTitle(String title);

	List<Item> searchByOclcnum(String oclcnum);

	List<Item> searchByPublisher(String publisher);

	List<Item> searchByForm(String form);

	List<Item> searchByCity(String city);

	Item acquire(String catalogNumber);
	
}
