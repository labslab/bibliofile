package com.gitlab.labslab.bibliofile.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.ISSN;
import com.gitlab.labslab.bibliofile.model.Item;

@Service
public class ItemServiceImpl extends AbstractCRUDService<Item> implements ItemService<Item> {
	@Autowired
	private ISBNService isbnService;
	@Autowired
	private ISSNService issnService;

	public List<Item> list() {
		List<Item> items = new ArrayList<>();
		Collection<ISSN> issns = issnService.list();
		Collection<ISBN> isbns = isbnService.list();

		if (issns != null && !issns.isEmpty()) {
			items.addAll(issns);
		}
		if (isbns != null && !isbns.isEmpty()) {
			items.addAll(isbns);
		}
		return items;
	}

	@Override
	public List<Item> searchByTitle(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByOclcnum(String oclcnum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByPublisher(String publisher) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByForm(String form) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Item acquire(String catalogNumber) {
		// TODO Auto-generated method stub
		return null;
	}

}
