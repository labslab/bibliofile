package com.gitlab.labslab.bibliofile.bo;

import java.util.Collection;

import com.gitlab.labslab.bibliofile.model.Item;
import com.gitlab.labslab.bibliofile.model.Loan;
import com.gitlab.labslab.bibliofile.model.User;

public interface LoanService extends CRUDService<Loan> {

	void end(Loan loan);

	void create(User user, Item item) throws LoanUnauthorizedException;

	Collection<Loan> findUserLoans(User user);

	void renew(Long loanId) throws Exception;

	void renew(Loan loan) throws Exception;

	void renew(User user, Loan loan) throws Exception;

}
