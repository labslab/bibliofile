package com.gitlab.labslab.bibliofile.bo;

import java.time.LocalDate;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.labslab.bibliofile.dao.LoanDao;
import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.ISSN;
import com.gitlab.labslab.bibliofile.model.Item;
import com.gitlab.labslab.bibliofile.model.ItemState;
import com.gitlab.labslab.bibliofile.model.Loan;
import com.gitlab.labslab.bibliofile.model.LoanState;
import com.gitlab.labslab.bibliofile.model.User;

@Service
public class LoanServiceImpl extends AbstractCRUDService<Loan>implements LoanService {

	@Autowired
	private UserService userService;

	@Autowired
	private ISBNService isbnService;

	@Autowired
	private ISSNService issnService;

	@Override
	@Transactional
	public void end(Loan loan) {
		loan.setState(LoanState.RETURNED);
		((LoanDao) super.getDao()).update(loan);
	}

	@Override
	@Transactional
	public void create(User user, Item item) throws LoanUnauthorizedException {
		if (item.getState() == ItemState.AVAILABLE) {
			if (user.isAllowed()) {
				Loan loan = new Loan();
				loan.setItem(item);
				item.setState(ItemState.BORROWED);
				if (item instanceof ISBN) {
					isbnService.update((ISBN) item);
				} else if (item instanceof ISSN) {
					issnService.update((ISSN) item);
				}
				loan.setDueDate(LocalDate.now().plusWeeks(2));
				loan.setUser(user);
				user.addLoan(loan);
				userService.update(user);
			} else {
				throw new LoanUnauthorizedException();
			}
		} else {
			throw new LoanUnauthorizedException("Livro indisponível");
		}
	}

	@Override
	public Collection<Loan> findUserLoans(User user) {
		return ((LoanDao) super.getDao()).findByUser(user);
	}

	@Override
	@Transactional
	public void renew(Long loanId) throws Exception {
		Loan loan = ((LoanDao) super.getDao()).findById(loanId);
		renew(loan);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void renew(Loan loan) throws Exception {
		User user = userService.loadById(loan.getUser().getId());
		if (user.isAllowed()) {
			loan.setDueDate(LocalDate.now().plusWeeks(2));
			((LoanDao) super.getDao()).update(loan);
		} else {
			throw new LoanUnauthorizedException();
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void renew(User user, Loan loan) throws Exception {
		if (user.isAllowed()) {
			loan.setDueDate(LocalDate.now().plusWeeks(2));
			((LoanDao) super.getDao()).update(loan);
		} else {
			throw new LoanUnauthorizedException();
		}
	}
}
