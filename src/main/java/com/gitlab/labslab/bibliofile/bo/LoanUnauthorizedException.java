package com.gitlab.labslab.bibliofile.bo;

@SuppressWarnings("serial")
public class LoanUnauthorizedException extends Exception {
	private final static String message = "O usuário não pode efetuar novos empréstimos com outros atrasados.";

	public LoanUnauthorizedException() {
		super(message);
	}

	public LoanUnauthorizedException(String string) {
		super(string);
	}

}
