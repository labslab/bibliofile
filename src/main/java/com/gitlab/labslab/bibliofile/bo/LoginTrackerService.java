package com.gitlab.labslab.bibliofile.bo;

import java.util.List;

import com.gitlab.labslab.bibliofile.model.LoginTracker;

public interface LoginTrackerService {
	
	List<LoginTracker> list();
	
	void save(LoginTracker loginTracker);

}
