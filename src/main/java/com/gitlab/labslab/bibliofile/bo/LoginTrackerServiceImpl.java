package com.gitlab.labslab.bibliofile.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.labslab.bibliofile.dao.LoginTrackerDao;
import com.gitlab.labslab.bibliofile.model.LoginTracker;

@Service
@Transactional(readOnly = true)
public class LoginTrackerServiceImpl implements LoginTrackerService {

	@Autowired
	private LoginTrackerDao dao;
	
	@Override
	public List<LoginTracker> list() {
		return dao.list();
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, readOnly = false)
	public void save(LoginTracker loginTracker) {
		dao.save(loginTracker);
	}

}
