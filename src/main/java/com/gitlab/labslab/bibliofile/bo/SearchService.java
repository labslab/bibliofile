package com.gitlab.labslab.bibliofile.bo;

import java.util.Collection;

import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.ISSN;
import com.gitlab.labslab.bibliofile.model.SearchForm;

public interface SearchService {

	Collection<ISBN> searchIsbn(SearchForm form);

	Collection<ISSN> searchIssn(SearchForm form);

}
