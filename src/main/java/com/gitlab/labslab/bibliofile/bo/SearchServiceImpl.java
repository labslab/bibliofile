package com.gitlab.labslab.bibliofile.bo;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.ISSN;
import com.gitlab.labslab.bibliofile.model.SearchField;
import com.gitlab.labslab.bibliofile.model.SearchForm;

@Service
public class SearchServiceImpl implements SearchService {

	@Autowired
	private ISBNService isbnService;

	@Override
	public Collection<ISSN> searchIssn(SearchForm form) { return null; }
//		if (form.getSearchString().isEmpty())
//			return ItemsController.getIssns();
//
//		Collection<ISSN> items = new HashSet<>();
//
//		for (SearchField f : form.getField()) {
//			switch (f) {
//
//			case CODE:
//				items.addAll(ItemsController.getIssns().parallelStream()
//						.filter(i -> i.getIssn().matches(".*(?ui)" + form.getSearchString() + ".*")
//								|| i.getIssnl().matches(".*(?ui)" + form.getSearchString() + ".*"))
//						.collect(Collectors.toSet()));
//				break;
//
//			case PUBLISHER:
//				items.addAll(ItemsController.getIssns().parallelStream()
//						.filter(i -> i.getPublisher().stream()
//								.anyMatch(p -> p.getName().matches(".*(?ui)" + form.getSearchString() + ".*")))
//						.collect(Collectors.toSet()));
//				break;
//
//			case TITLE:
//				items.addAll(ItemsController.getIssns().parallelStream()
//						.filter(i -> i.getTitle().matches(".*(?ui)" + form.getSearchString() + ".*"))
//						.collect(Collectors.toSet()));
//				break;
//
//			default:
//				break;
//			}
//		}
//		return items;
//	}

	@Override
	public Collection<ISBN> searchIsbn(SearchForm form) {
//		if (form.getSearchString().isEmpty())
//			return ItemsController.getIsbns();
		
		Collection<ISBN> items = new HashSet<>();

		for (SearchField f : form.getField()) {
			switch (f) {

			case CODE:
				items.add(isbnService.searchByISBN(form.getSearchString()));
//				items.addAll(ItemsController.getIsbns().parallelStream()
//						.filter(i -> i.getCode().stream()
//								.anyMatch(p -> p.getCode().matches(".*(?ui)" + form.getSearchString() + ".*")))
//						.collect(Collectors.toSet()));
				break;

			case AUTHOR:
				items.addAll(isbnService.searchByAuthor(form.getSearchString()));
//				items.addAll(ItemsController.getIsbns().parallelStream()
//						.filter(i -> i.getAuthor().stream()
//								.anyMatch(a -> a.getName().matches(".*(?ui)" + form.getSearchString() + ".*")))
//						.collect(Collectors.toSet()));
				break;

			case PUBLISHER:
				items.addAll(isbnService.searchByPublisher(form.getSearchString()));
//				items.addAll(ItemsController.getIsbns().parallelStream()
//						.filter(i -> i.getPublisher().stream()
//								.anyMatch(p -> p.getName().matches(".*(?ui)" + form.getSearchString() + ".*")))
//						.collect(Collectors.toSet()));
				break;

			case TITLE:
				items.addAll(isbnService.searchByTitle(form.getSearchString()));
//				items.addAll(ItemsController.getIsbns().parallelStream()
//						.filter(i -> i.getTitle().matches(".*(?ui)" + form.getSearchString() + ".*"))
//						.collect(Collectors.toSet()));
				break;

			default:
				break;
			}
		}
		return items;
	}
}
