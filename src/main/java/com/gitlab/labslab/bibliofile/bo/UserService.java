package com.gitlab.labslab.bibliofile.bo;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.gitlab.labslab.bibliofile.bo.UserServiceImpl.UserExistsException;
import com.gitlab.labslab.bibliofile.model.User;
import com.gitlab.labslab.bibliofile.model.UserDto;

public interface UserService extends UserDetailsService, CRUDService<User>{

	boolean comparePass(String passAttempt, User user);

	void save(List<User> users);

	User createUser(UserDto userTmp) throws UserExistsException;
	
	User find(String username);

}
