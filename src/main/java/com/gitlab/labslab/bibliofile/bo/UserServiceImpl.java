package com.gitlab.labslab.bibliofile.bo;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.labslab.bibliofile.dao.RoleDao;
import com.gitlab.labslab.bibliofile.dao.UserDao;
import com.gitlab.labslab.bibliofile.model.Role;
import com.gitlab.labslab.bibliofile.model.User;
import com.gitlab.labslab.bibliofile.model.UserDto;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl extends AbstractCRUDService<User>implements UserService {

	@SuppressWarnings(value = "serial")
	public class UserExistsException extends Exception {

	}

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private RoleDao roleDao;

	private final String encode(String password) {
		return encoder.encode(password);
	}

	@Override
	public boolean comparePass(String passAttempt, User user) {
		return encoder.matches(passAttempt, user.getPassword());
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
	public void save(List<User> users) {
		users.stream().forEach(user -> ((UserDao) super.getDao()).save(user));
	}

	@Override
	public User find(String username) {
		return (User) ((UserDao) super.getDao()).loadUserByUsername(username);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return ((UserDao) super.getDao()).loadUserByUsername(username);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
	public User createUser(UserDto userTmp) throws UserExistsException {
		User user = null;
		try {
			user = (User) ((UserDao) super.getDao()).loadUserByUsername(userTmp.getUsername());
		} catch (Exception e) {

		}
		if (user == null) {
			user = new User();
			user.setUsername(userTmp.getUsername());
			user.setPassword(encode(userTmp.getPassword().getOriginal()));
			user.setRoles(userTmp.getRoles());
			if (userTmp.getRoles() == null || userTmp.getRoles().isEmpty()) {
				Collection<Role> roles = new HashSet<>();
				Role r1 = roleDao.findByAuth("ROLE_USER");
				roles.add(r1);
				user.setRoles(roles);
			} else {
				user.setRoles(userTmp.getRoles());
			}

			((UserDao) super.getDao()).save(user);

			return user;
		} else {
			throw new UserExistsException();
		}
	}

}
