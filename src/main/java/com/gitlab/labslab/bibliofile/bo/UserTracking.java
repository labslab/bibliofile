package com.gitlab.labslab.bibliofile.bo;

import java.util.HashMap;
import java.util.Map;

import com.gitlab.labslab.bibliofile.model.User;

public class UserTracking {
	
	private static Map<Long, User> loggedInUsers = new HashMap<>();

	public void addUser(User user) {
		loggedInUsers.put(user.getId(), user);
	}

	public void removeUser(User user) {
		loggedInUsers.remove(user.getId());
	}

	public static Map<Long, User> getLoggedInUsers() {
		return loggedInUsers;
	}

}
