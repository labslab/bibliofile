package com.gitlab.labslab.bibliofile.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.gitlab.labslab.bibliofile.bo.UserService;
import com.gitlab.labslab.bibliofile.bo.UserTracking;
import com.gitlab.labslab.bibliofile.model.LoginAction;
import com.gitlab.labslab.bibliofile.model.LoginTracker;
import com.gitlab.labslab.bibliofile.model.User;

@Component(value = "authSuccessHandlerImpl")
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

	@Autowired
	private UserService service;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
			throws IOException, ServletException {
		try {
			User user = (User) auth.getPrincipal();
			UserTracking userTracker = new UserTracking();
			userTracker.addUser(user);
			LoginTracker loginTracker = new LoginTracker(LoginAction.LOGIN);
			user.addLoginTracker(loginTracker);
			service.update(user);
			user.getLoginTrack().remove(loginTracker);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			res.sendRedirect(req.getRequestURL().toString());
		}
	}

}
