package com.gitlab.labslab.bibliofile.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

import com.gitlab.labslab.bibliofile.bo.UserService;
import com.gitlab.labslab.bibliofile.bo.UserTracking;
import com.gitlab.labslab.bibliofile.model.LoginAction;
import com.gitlab.labslab.bibliofile.model.LoginTracker;
import com.gitlab.labslab.bibliofile.model.User;

@Component(value = "customLogout")
public class CustomLogout extends SecurityContextLogoutHandler {

	@Autowired
	@Qualifier("userServiceImpl")
	private UserService service;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		try {
			User user = (User) authentication.getPrincipal();
			LoginTracker tracker = new LoginTracker(LoginAction.LOGOUT);
			user.addLoginTracker(tracker);
			service.update(user);
			UserTracking ut = new UserTracking();
			ut.removeUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			super.logout(request, response, authentication);
		}

	}

}
