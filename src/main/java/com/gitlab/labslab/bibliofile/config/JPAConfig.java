package com.gitlab.labslab.bibliofile.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@EnableTransactionManagement
public class JPAConfig {

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

		JpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		ComboPooledDataSource cpds = new ComboPooledDataSource();
		cpds.setUser("hibernate");
		cpds.setPassword("");
		cpds.setJdbcUrl("jdbc:mysql://localhost:3306/bibliofile?useSSL=false&serverTimezone=America/Sao_Paulo");
		cpds.setInitialPoolSize(5);
		cpds.setMinPoolSize(5);
		cpds.setMaxPoolSize(20);
		cpds.setMaxIdleTime(300);
		cpds.setUnreturnedConnectionTimeout(30);

		try {
			cpds.setDriverClass("com.mysql.cj.jdbc.Driver");
		} catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factoryBean.setDataSource(cpds);

		Properties props = new Properties();
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		props.setProperty("hibernate.show_sql", "true");
		props.setProperty("hibernate.hbm2ddl.auto", "update");
		props.setProperty("characterEncoding", "UTF-8");
		props.setProperty("hibernate.id.new_generator_mappings", "false");
		props.setProperty("useJDBCCompliantTimezoneShift", "true");
		props.setProperty("useLegacyDatetimeCode", "false");

		factoryBean.setJpaProperties(props);

		factoryBean.setPackagesToScan("com.gitlab.labslab.bibliofile.model");

		return factoryBean;

	}

	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
		return new JpaTransactionManager(emf);
	}
}
