package com.gitlab.labslab.bibliofile.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.gitlab.labslab.bibliofile.bo.UserService;
import com.gitlab.labslab.bibliofile.model.User;

@Configuration
@EnableWebSecurity
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Qualifier("userServiceImpl")
	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier("authSuccessHandlerImpl")
	private AuthenticationSuccessHandler authSuccess;

	@Autowired
	@Qualifier("customLogout")
	private LogoutHandler customLogout;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/emprestimos").hasRole("USER")
				.antMatchers("/issn/registrar/*").hasRole("LIBRARIAN")
				.antMatchers("/isbn/registrar/*").hasRole("LIBRARIAN")
				.antMatchers("/registrar-item").hasRole("LIBRARIAN")
				.antMatchers("/itens/**").permitAll()
				.antMatchers("/emprestimo/efetuar").hasRole("LIBRARIAN")
				.antMatchers("/emprestimo/").hasRole("USER")
				.antMatchers("/emprestimo/renovar").hasRole("USER")
				.antMatchers(HttpMethod.GET, "/").permitAll()
				.antMatchers("/usuario/cadastro").permitAll()
				.antMatchers("/test/**").permitAll()
				.antMatchers("/usuario/lista").hasRole("USER")
				.antMatchers("/busca/*").permitAll()
				.antMatchers(HttpMethod.GET, "/isbn/acquire").permitAll()
				.antMatchers(HttpMethod.GET, "/issn/acquire").permitAll()
				.antMatchers(HttpMethod.GET, "/populate/*").permitAll()
				.antMatchers(HttpMethod.GET, "/resources/**").permitAll()
				.anyRequest().authenticated()
				.and().formLogin().loginPage("/login").permitAll().successHandler(authSuccess)
				.and().logout().addLogoutHandler(customLogout).logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
		super.configure(http);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public AuditorAware<User> auditorProvider() {
		return new SpringSecurityAuditorAware();
	}
}
