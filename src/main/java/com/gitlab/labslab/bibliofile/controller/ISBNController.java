package com.gitlab.labslab.bibliofile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gitlab.labslab.bibliofile.bo.ISBNService;
import com.gitlab.labslab.bibliofile.model.ISBN;

@Controller
@RequestMapping(value = "/isbn")
public class ISBNController {

	@Autowired
	private ISBNService service;

	@RequestMapping(value = "/acquire", method = RequestMethod.GET)
	public ModelAndView fromJson(@RequestParam String isbn) {
		ISBN book = service.acquire(isbn);
		if (book == null)
			book = new ISBN();
		return new ModelAndView("registrarIsbn", "item", book);
	}

	@RequestMapping(value = "/registrar", method = RequestMethod.GET)
	public ModelAndView isbn() {
		ISBN isbn = new ISBN();
		return new ModelAndView("registrarIsbn", "item", isbn);
	}

	@RequestMapping(value = "/registrar/{code}", method = RequestMethod.GET)
	public ModelAndView populateIsbn(@PathVariable("code") String code) {
		ISBN isbn;
		isbn = service.acquire(code);
		return new ModelAndView("registrarIsbn", "item", isbn);
	}

	@RequestMapping(value = "/registrar", method = RequestMethod.POST)
	public ModelAndView registrar(@ModelAttribute(value = "item") ISBN isbn, BindingResult br,
			final RedirectAttributes redirectAttribute) {
		try {
			service.save(isbn);
			redirectAttribute.addFlashAttribute("success",
					String.format("Livro \"%s\" cadastrado com sucesso!", isbn.getTitle()));
		} catch (NullPointerException e) {
			e.printStackTrace();
			// TODO: handle exception

		} catch (Exception e) {
			redirectAttribute.addFlashAttribute("error", "Item não cadastrado. =(<br/>" + e.getMessage());
			e.printStackTrace();
		}

		return new ModelAndView("redirect:/registrar-item");
	}

}
