package com.gitlab.labslab.bibliofile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gitlab.labslab.bibliofile.bo.ISSNService;
import com.gitlab.labslab.bibliofile.model.ISSN;

@Controller
@RequestMapping(value = "/issn")
public class ISSNController {

	@Autowired
	private ISSNService service;

	@RequestMapping(value = "/acquire", method = RequestMethod.GET)
	public ModelAndView fromJson(@RequestParam String issn) {
		ISSN item = service.acquire(issn);
		if (item == null)
			item = new ISSN();

		return new ModelAndView("registrarIssn", "item", item);
	}

	@RequestMapping(value = "/registrar", method = RequestMethod.GET)
	public ModelAndView issn() {
		ISSN issn = new ISSN();
		return new ModelAndView("registrarIssn", "item", issn);
	}

	@RequestMapping(value = "/registrar/{code}", method = RequestMethod.GET)
	public ModelAndView populateIssn(@PathVariable("code") String code) {
		ISSN issn;
		issn = service.acquire(code);
		return new ModelAndView("registrarIssn", "item", issn);
	}

	@RequestMapping(value = "/registrar", method = RequestMethod.POST)
	public ModelAndView registrar(@ModelAttribute(value = "issn") ISSN issn, BindingResult br,
			final RedirectAttributes redirectAttribute) {
		try {
			service.save(issn);
			redirectAttribute.addFlashAttribute("success", "Periódico cadastrado com sucesso!");
		} catch (Exception e) {
			redirectAttribute.addFlashAttribute("error", "Item não cadastrado. =(<br/>" + e.getMessage());
		}

		return new ModelAndView("redirect:/registrar-item");
	}
}