package com.gitlab.labslab.bibliofile.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gitlab.labslab.bibliofile.bo.ISBNService;
import com.gitlab.labslab.bibliofile.bo.ISSNService;
import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.ISSN;
import com.gitlab.labslab.bibliofile.model.Item;

@Controller
@RequestMapping(value = "/itens")
public class ItemsController {


	@Autowired
	private ISBNService isbnService;
	@Autowired
	private ISSNService issnService;

	private void populateItems(Collection<Item> items) {
		try {
			Collection<ISSN> journals = issnService.list();
			if (journals != null && !journals.isEmpty())
				items.addAll(journals);
		} catch (Exception e) {
			System.out.println("populate issn failed =(");
			e.printStackTrace();
		}
		try {
			Collection<ISBN> books = isbnService.list();
			if (books != null && !books.isEmpty())
				items.addAll(books);
		} catch (Exception e) {
			System.out.println("populate isbn failde =(");
			e.printStackTrace();
		}
	}

	public Collection<Item> getItems() {
		List<Item> items = new ArrayList<>();
		populateItems(items);
		return items;

	}
	@RequestMapping(value = "/detalhe/{id}", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Long id) {
		ISBN item = isbnService.loadById(id);
		return new ModelAndView("details", "item", item);
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public ModelAndView list() {
		return new ModelAndView("items", "items", getItems());
	}

}
