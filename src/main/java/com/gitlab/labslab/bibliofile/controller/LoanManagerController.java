package com.gitlab.labslab.bibliofile.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gitlab.labslab.bibliofile.bo.ISBNService;
import com.gitlab.labslab.bibliofile.bo.LoanService;
import com.gitlab.labslab.bibliofile.bo.UserService;
import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.Loan;
import com.gitlab.labslab.bibliofile.model.User;

@Controller
@RequestMapping("/emprestimo")
public class LoanManagerController {

	@Autowired
	private LoanService service;

	@Autowired
	private UserService userService;

	@Autowired
	private ISBNService isbnService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView loans() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Collection<Loan> loans = service.findUserLoans(user);
		user.setLoans(loans);
		ModelAndView mav = new ModelAndView("loans", "loans", loans);
		mav.addObject("user", user);
		return mav;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView newLoan(@PathVariable("id") Long id) {
		ISBN item = isbnService.loadById(id);
		return new ModelAndView("loan", "item", item);
	}

	@RequestMapping(value = "/renovar", method = RequestMethod.GET)
	public ModelAndView renewLoan(@RequestParam(value = "idLoan") Long idLoan, RedirectAttributes redir) {
		try {
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Collection<Loan> loans = service.findUserLoans(user);
			user.setLoans(loans);
			Loan loan = service.loadById(idLoan);
			service.renew(user, loan);
			redir.addFlashAttribute("success",
					String.format("Renovação de empréstimo do livro \"%s\" efetuado com sucesso para o usuário %s! =)",
							loan.getItem().getTitle(), loan.getUser().getUsername()));
		} catch (Exception e) {
			redir.addFlashAttribute("error", e.getMessage());
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/efetuar", method = RequestMethod.GET)
	public ModelAndView createLoan(@RequestParam(value = "idItem") Long idItem,
			@RequestParam(value = "username") String username, RedirectAttributes redir) {
		try {
			ISBN item = isbnService.loadById(idItem);
			User user = userService.find(username);
			service.create(user, item);
			redir.addFlashAttribute("success",
					String.format("Empréstimo efetuado com sucesso para usuario %s! =)", user.getUsername()));
		} catch (Exception e) {
			redir.addFlashAttribute("error", "Falha na criação do empréstimo<br>" + e.getMessage());
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/");
	}
}
