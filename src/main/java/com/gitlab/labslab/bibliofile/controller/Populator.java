package com.gitlab.labslab.bibliofile.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.labslab.bibliofile.bo.UserService;
import com.gitlab.labslab.bibliofile.bo.UserServiceImpl.UserExistsException;
import com.gitlab.labslab.bibliofile.dao.RoleDao;
import com.gitlab.labslab.bibliofile.model.Password;
import com.gitlab.labslab.bibliofile.model.Role;
import com.gitlab.labslab.bibliofile.model.User;
import com.gitlab.labslab.bibliofile.model.UserDto;

@RestController
@Transactional
@RequestMapping(value = "/populate")
public class Populator {

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public Set<Role> roles() {
		Set<Role> roles = new HashSet<>();

		Role r1 = new Role();
		r1.setAuthority("ROLE_ADMIN");
		roleDao.save(r1);
		roles.add(r1);

		Role r2 = new Role();
		r2.setAuthority("ROLE_USER");
		roleDao.save(r2);
		roles.add(r2);

		Role r3 = new Role();
		r3.setAuthority("ROLE_LIBRARIAN");
		roleDao.save(r3);
		roles.add(r3);

		return roles;
	}

	@RequestMapping(value = "/bibliotecario")
	public User librarian() {
		UserDto librarian = new UserDto();
		librarian.setUsername("bibliotecario");
		Password p = new Password();
		p.setConfirmation("1234");
		p.setOriginal("1234");
		librarian.setPassword(p);
		Collection<Role> roles = new HashSet<>();
		roles.add(roleDao.findByAuth("ROLE_LIBRARIAN"));
		roles.add(roleDao.findByAuth("ROLE_USER"));
		librarian.setRoles(roles);
		User newUser = null;
		try {
			newUser = userService.createUser(librarian);
		} catch (UserExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newUser;
	}

	@RequestMapping(value = "/admin")
	public User admin() {
		UserDto admin = new UserDto();
		admin.setUsername("admin");
		Password p = new Password();
		p.setConfirmation("1234");
		p.setOriginal("1234");
		admin.setPassword(p);
		Collection<Role> roles = new HashSet<>();
		roles.add(roleDao.findByAuth("ROLE_ADMIN"));
		roles.add(roleDao.findByAuth("ROLE_USER"));
		admin.setRoles(roles);
		User newUser = null;
		try {
			newUser = userService.createUser(admin);
		} catch (UserExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newUser;
	}
}
