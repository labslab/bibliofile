package com.gitlab.labslab.bibliofile.controller;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Service
@RequestMapping(value = "/registrar-item")
public class RegisterItemController {

	@RequestMapping(method = RequestMethod.GET)
	public String main() {
		return "registrarItem";
	}

}
