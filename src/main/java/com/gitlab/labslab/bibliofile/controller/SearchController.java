package com.gitlab.labslab.bibliofile.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gitlab.labslab.bibliofile.bo.SearchService;
import com.gitlab.labslab.bibliofile.model.Item;
import com.gitlab.labslab.bibliofile.model.SearchField;
import com.gitlab.labslab.bibliofile.model.SearchForm;

@RequestMapping(value = "/busca")
@Controller
public class SearchController {
	@Autowired
	private SearchService searchService;

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView main() {
		SearchForm form = new SearchForm();
		SearchField[] fieldsArray = SearchField.values();
		Map<SearchField, String> fields = new HashMap<>();
		for (SearchField f : fieldsArray) {
			fields.put(f, f.getName());
		}
		ModelAndView mav = new ModelAndView("searchItem");
		mav.addObject("searchForm", form);
		mav.addObject("fields", fields);
		return mav;
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute(value = "searchForm") SearchForm form, BindingResult br) {
		Collection<Item> items = new HashSet<>();
		// items.addAll(searchIssn(form));
		items.addAll(searchService.searchIsbn(form));

		return new ModelAndView("items", "items", items);
	}

	/* implementar paginação */
	@ResponseBody
//	@RequestMapping(method = RequestMethod.POST)
	ModelAndView searchNew(@RequestParam(required = false) String page,
			@ModelAttribute(value = "searchForm") SearchForm form, BindingResult br, HttpSession session) {
		// public ModelAndView search(@ModelAttribute(value = "searchForm")
		// SearchForm form, BindingResult br) {
		
		Collection<Item> items = new HashSet<>();
		// items.addAll(searchIssn(form));
		items.addAll(searchService.searchIsbn(form));

		return new ModelAndView("items", "items", items);
	}

}
