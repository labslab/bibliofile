package com.gitlab.labslab.bibliofile.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.persistence.TemporalType;

import org.springframework.format.datetime.standard.DateTimeFormatterFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.labslab.bibliofile.model.Author;
import com.gitlab.labslab.bibliofile.model.Code;
import com.gitlab.labslab.bibliofile.model.Form;
import com.gitlab.labslab.bibliofile.model.ISBN;
import com.gitlab.labslab.bibliofile.model.ItemState;
import com.gitlab.labslab.bibliofile.model.Oclcnum;

@RestController
@RequestMapping(value = "/test")
public class TestsController {

	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public Map<String, Object> testCreateBooks(@RequestParam("n") int n) {
		LocalTime time1 = LocalTime.now();
		Collection<ISBN> books = new HashSet<>();
		for (int i = 0; i < n; i++) {
			ISBN book = new ISBN();
			List<Author> authors = new ArrayList<>();
			authors.add(new Author(String.valueOf(new Random().nextInt())));
			book.setAuthor(authors);
			book.setCity(String.valueOf(new Random().nextInt()));
			List<Code> codes = new ArrayList<>();
			codes.add(new Code(String.valueOf(new Random().nextInt())));
			book.setCode(codes);
			book.setTitle(String.valueOf(new Random().nextInt()));
			book.setEd(String.valueOf(new Random().nextInt()));
			book.addForm(new Form(String.valueOf(new Random().nextInt())));
			book.setLang(String.valueOf(new Random().nextInt()));
			List<Oclcnum> oclcnum = new ArrayList<>();
			oclcnum.add(new Oclcnum(String.valueOf(new Random().nextInt())));
			book.setOclcnum(oclcnum);
			book.setState(ItemState.values()[new Random().nextInt(3)]);
			book.setYear(LocalDate.now().minusDays(new Random().nextInt(100000)));
			books.add(book);

		}
		Map<String, Object> responseMap = new HashMap<>();
		LocalTime time2 = LocalTime.now();
		responseMap.put("TEMPO1" , time1);
		responseMap.put("TEMPO2", time2);
		responseMap.put("BOOKS", books);
		return responseMap;
		

	}
}
