package com.gitlab.labslab.bibliofile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gitlab.labslab.bibliofile.bo.UserService;
import com.gitlab.labslab.bibliofile.bo.UserServiceImpl.UserExistsException;
import com.gitlab.labslab.bibliofile.bo.UserTracking;
import com.gitlab.labslab.bibliofile.model.UserDto;

@Controller
@RequestMapping(value = "/usuario")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public ModelAndView main() {
		return new ModelAndView("newUser", "user", new UserDto());
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String register(@ModelAttribute("user") UserDto userDto, BindingResult res, RedirectAttributes redir) {
		
		if (userDto.getPassword().isValid()) {

			try {
				userService.createUser(userDto);
				redir.addFlashAttribute("success", "Usuário cadastrado com sucesso!");
			} catch (UserExistsException e) {
				redir.addFlashAttribute("error", "Usuário já existe!");
			}
		} else {
			redir.addFlashAttribute("error", "Senhas não batem!");
		}
		return "redirect:/";
	}
	
	@RequestMapping(value = "/lista", method = RequestMethod.GET)
	public ModelAndView listar() {
		
		return new ModelAndView("users", "users", UserTracking.getLoggedInUsers().values()); 
	}

}
