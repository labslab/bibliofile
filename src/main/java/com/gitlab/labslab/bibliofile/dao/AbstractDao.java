package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractDao<T> implements Dao<T> {

	private final Class<T> genericType;

	@PersistenceContext
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	protected AbstractDao() {
		this.genericType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractDao.class);
	}

	protected EntityManager getManager() {
		return manager;
	}

	@Override
	public List<T> list() {
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<T> q = cb.createQuery(genericType);
		Root<T> root = q.from(genericType);
		return manager.createQuery(q.select(root)).getResultList();

	}

	@Override
	public T findById(long id) {
		return manager.find(genericType, id);
	}

	@Override
	public void removeById(long id) {
		manager.remove(findById(id));

	}

	@Override
	public void remove(T obj) {
		manager.remove(obj);

	}

	@Override
	public void update(T obj) {
		manager.merge(obj);

	}

	@Override
	public void save(T obj) {
		manager.persist(obj);

	}

}
