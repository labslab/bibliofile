package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import com.gitlab.labslab.bibliofile.model.Author;

public interface AuthorDao extends Dao<Author> {
	Author findByExactName(String name);

	List<Author> findByName(String name);

}
