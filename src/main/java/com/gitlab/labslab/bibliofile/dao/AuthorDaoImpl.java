package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Author;

@Repository
public class AuthorDaoImpl extends AbstractDao<Author> implements AuthorDao {

	@Override
	public Author findByExactName(String name) {
		List<Author> authors = super.getManager().createQuery("SELECT a FROM Author a WHERE a.name = :name", Author.class)
				.setParameter("name", name).getResultList();
		if (authors.isEmpty()) {
			return null;
		}
		return authors.get(0);
	}

	@Override
	public List<Author> findByName(String name) {
		return super.getManager().createQuery("SELECT a FROM Author a WHERE UPPER(a.name) LIKE UPPER(:name)", Author.class)
				.setParameter("name", "%" + name + "%").getResultList();
	}

}
