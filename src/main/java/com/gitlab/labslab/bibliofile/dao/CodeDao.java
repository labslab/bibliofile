package com.gitlab.labslab.bibliofile.dao;

import com.gitlab.labslab.bibliofile.model.Code;

public interface CodeDao extends Dao<Code> {
	Code findByCode(String code);
}
