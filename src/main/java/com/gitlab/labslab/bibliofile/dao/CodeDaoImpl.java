package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Code;

@Repository
public class CodeDaoImpl extends AbstractDao<Code> implements CodeDao {


	@Override
	public Code findByCode(String code) {
		List<Code> codes = super.getManager().createQuery("SELECT c FROM Code c WHERE c.code = :code", Code.class)
				.setParameter("code", code)
				.getResultList();
		if (codes.isEmpty()) {
			return null;
		}
		return codes.get(0);
	}

}
