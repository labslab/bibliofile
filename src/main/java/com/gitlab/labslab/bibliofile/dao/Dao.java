/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

/**
 *
 * @author boxx
 */
public interface Dao<T> {

    List<T> list();

    T findById(long id);

    void removeById(long id);

    void remove(T obj);

    void update(T obj);

    void save(T obj);
}
