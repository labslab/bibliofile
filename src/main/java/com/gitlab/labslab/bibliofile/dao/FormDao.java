package com.gitlab.labslab.bibliofile.dao;

import com.gitlab.labslab.bibliofile.model.Form;

public interface FormDao extends Dao<Form> {
	
	Form findByName(String name);
}
