package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Form;

@Repository
public class FormDaoImpl extends AbstractDao<Form> implements FormDao {

	@Override
	public Form findByName(String name) {
		List<Form> forms = super.getManager().createQuery("SELECT f FROM Form f WHERE f.name = :name", Form.class)
				.setParameter("name", name)
				.getResultList();
		if(forms.isEmpty()) {
			return null;
		}
		return forms.get(0);
	}

}
