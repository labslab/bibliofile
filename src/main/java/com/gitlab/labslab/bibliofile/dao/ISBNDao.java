package com.gitlab.labslab.bibliofile.dao;

import java.util.Calendar;
import java.util.List;

import com.gitlab.labslab.bibliofile.model.ISBN;

public interface ISBNDao extends ItemDao <ISBN>{

	List<ISBN> searchByAuthor(String author);

	ISBN searchByISBN(String isbn);

	List<ISBN> searchByLanguage(String language);
	
	List<ISBN> searchByYear(Calendar year);

}
