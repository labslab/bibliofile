package com.gitlab.labslab.bibliofile.dao;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.ISBN;

@Repository
public class ISBNDaoImpl extends AbstractDao<ISBN> implements ISBNDao {

	@Override
	public List<ISBN> searchByTitle(String title) {
		return super.getManager()
				.createQuery("SELECT i FROM ISBN i WHERE UPPER(i.title) LIKE UPPER(:title)", ISBN.class)
				.setParameter("title", "%" + title + "%").getResultList();
	}

	@Override
	public List<ISBN> searchByOclcnum(String oclcnum) {
		return super.getManager().createQuery("SELECT i FROM ISBN i JOIN FETCH i.oclnum o WHERE o.num = :oclcnum)", ISBN.class)
				.setParameter("oclcnum", oclcnum).getResultList();
	}

	@Override
	public List<ISBN> searchByPublisher(String publisher) {
		return super.getManager()
				.createQuery("SELECT i FROM ISBN i JOIN FETCH i.publisher p WHERE UPPER(p.name) LIKE UPPER(:publisher)", ISBN.class)
				.setParameter("publisher", "%" + publisher + "%").getResultList();
	}

	@Override
	public List<ISBN> searchByForm(String form) {
		return super.getManager()
				.createQuery("SELECT i FROM ISBN i JOIN FETCH i.form f WHERE UPPER(f.name) LIKE UPPER(:form)", ISBN.class)
				.setParameter("form", "%" + form + "%").getResultList();
	}

	@Override
	public List<ISBN> searchByCity(String city) {
		return super.getManager().createQuery("SELECT i FROM ISBN i WHERE UPPER(i.city) LIKE UPPER(:city)", ISBN.class)
				.setParameter("city", "%" + city + "%").getResultList();
	}

	@Override
	public List<ISBN> searchByAuthor(String author) {
		return super.getManager()
				.createQuery("SELECT i FROM ISBN i JOIN FETCH i.author a WHERE UPPER(a.name) LIKE UPPER(:author)", ISBN.class)
				.setParameter("author", "%" + author + "%").getResultList();
	}

	@Override
	public ISBN searchByISBN(String isbn) {
		List<ISBN> result = super.getManager().createQuery("SELECT i FROM ISBN i JOIN FETCH i.code c WHERE c.code = :isbn", ISBN.class)
				.setParameter("isbn", isbn).getResultList();
		if (result == null || result.isEmpty()) {
			return null;
		} else {
			return result.get(0);
		}
	}

	@Override
	public List<ISBN> searchByLanguage(String language) {
		return super.getManager().createQuery("SELECT i FROM ISBN i WHERE i.lang = :language", ISBN.class)
				.setParameter("language", language).getResultList();
	}

	@Override
	public List<ISBN> searchByYear(Calendar year) {
		return super.getManager().createQuery("SELECT i FROM ISBN i WHERE i.year = :year", ISBN.class)
				.setParameter("year", year.get(Calendar.YEAR)).getResultList();
	}

}
