package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import com.gitlab.labslab.bibliofile.model.ISSN;

public interface ISSNDao extends ItemDao<ISSN> {

	ISSN searchByIssnl(String issnl);

	ISSN searchByIssn(String issn);

	List<ISSN> searchByRawCoverage(String rawCoverage);
}
