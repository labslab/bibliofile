package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.ISSN;

@Repository
public class ISSNDaoImpl extends AbstractDao<ISSN> implements ISSNDao {

	@Override
	public List<ISSN> searchByTitle(String title) {
		return super.getManager()
				.createQuery("SELECT i FROM ISSN i WHERE UPPER(i.title) LIKE UPPER(:title)", ISSN.class)
				.setParameter("title", "%" + title + "%").getResultList();
	}

	@Override
	public List<ISSN> searchByOclcnum(String oclcnum) {
		return super.getManager().createQuery("SELECT i FROM ISSN i WHERE i.oclcnum = :oclcnum", ISSN.class)
				.setParameter("oclcnum", oclcnum).getResultList();
	}

	@Override
	public List<ISSN> searchByPublisher(String publisher) {
		return super.getManager()
				.createQuery("SELECT i FROM ISSN i WHERE UPPER(i.publisher) LIKE UPPER(:publisher)", ISSN.class)
				.setParameter("publisher", "%" + publisher + "%").getResultList();
	}

	@Override
	public List<ISSN> searchByForm(String form) {
		return super.getManager()
				.createQuery("SELECT i FROM ISSN i WHERE UPPER(i.form.name) = UPPER(:form)", ISSN.class)
				.setParameter("form", form).getResultList();
	}

	@Override
	public List<ISSN> searchByCity(String city) {
		return super.getManager().createQuery("SELECT i FROM ISSN i WHERE UPPER(i.city.name) LIKE (:city)", ISSN.class)
				.setParameter("city", "%" + city + "%").getResultList();
	}

	@Override
	public ISSN searchByIssnl(String issnl) {
		List<ISSN> result = super.getManager().createQuery("SELECT i FROM ISBN i WHERE i.code = :issnl", ISSN.class)
				.setParameter("issnl", issnl).getResultList();
		if (result == null || result.isEmpty()) {
			return null;
		} else {
			return result.get(0);
		}
	}

	@Override
	public ISSN searchByIssn(String issn) {
		List<ISSN> result = super.getManager().createQuery("SELECT i FROM ISSN i WHERE i.code = :issn", ISSN.class)
				.setParameter("issn", issn).getResultList();
		if (result == null || result.isEmpty()) {
			return null;
		} else {
			return result.get(0);
		}
	}

	@Override
	public List<ISSN> searchByRawCoverage(String rawCoverage) {
		return super.getManager()
				.createQuery("SELECT i FROM ISSN i WHERE UPPER(i.rawCoverage) LIKE UPPER(:rawCoverage)", ISSN.class)
				.setParameter("rawCoverage", "%" + rawCoverage + "%").getResultList();
	}

}
