package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

public interface ItemDao<Item> extends Dao<Item> {

	List<Item> searchByTitle(String title);

	List<Item> searchByOclcnum(String oclcnum);

	List<Item> searchByPublisher(String publisher);

	List<Item> searchByForm(String form);

	List<Item> searchByCity(String city);
}
