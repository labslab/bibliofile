package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Item;

@Repository
public class ItemDaoImpl extends AbstractDao<Item> implements ItemDao<Item> {


	@Override
	public List<Item> searchByTitle(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByOclcnum(String oclcnum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByPublisher(String publisher) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByForm(String form) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Item> searchByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

}