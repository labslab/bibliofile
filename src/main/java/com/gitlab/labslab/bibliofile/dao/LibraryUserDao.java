package com.gitlab.labslab.bibliofile.dao;

import com.gitlab.labslab.bibliofile.model.LibraryUser;

public interface LibraryUserDao extends Dao<LibraryUser> {
	

}
