package com.gitlab.labslab.bibliofile.dao;

import java.util.Calendar;
import java.util.Collection;

import com.gitlab.labslab.bibliofile.model.Loan;
import com.gitlab.labslab.bibliofile.model.LoanState;
import com.gitlab.labslab.bibliofile.model.User;

public interface LoanDao extends Dao<Loan> {

	Collection<Loan> findByState(LoanState state);

	Collection<Loan> findByUser(User user);

	Collection<Loan> findByUserId(Long id);

	Collection<Loan> findByDueDate(Calendar due);

	Collection<Loan> findLateFromUser(User user);

	Collection<Loan> findLate();

	Collection<Loan> findByCreationDate(Calendar created);

}
