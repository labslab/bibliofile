package com.gitlab.labslab.bibliofile.dao;

import java.util.Calendar;
import java.util.Collection;

import javax.persistence.TemporalType;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Loan;
import com.gitlab.labslab.bibliofile.model.LoanState;
import com.gitlab.labslab.bibliofile.model.User;

@Repository
public class LoanDaoImpl extends AbstractDao<Loan> implements LoanDao {


	@Override
	public Collection<Loan> findByUser(User user) {
		return super.getManager().createQuery("SELECT l FROM Loan l WHERE l.user = :user", Loan.class).setParameter("user", user)
				.getResultList();
	}

	@Override
	public Collection<Loan> findByState(LoanState state) {
		return super.getManager().createQuery("SELECT l FROM Loan l WHERE l.state = :state", Loan.class)
				.setParameter("state", state).getResultList();
	}

	@Override
	public Collection<Loan> findByUserId(Long id) {
		return super.getManager().createQuery("SELECT l FROM Loan l WHERE l.user.id = :id", Loan.class).setParameter("id", id)
				.getResultList();
	}

	@Override
	public Collection<Loan> findByDueDate(Calendar due) {
		return super.getManager().createQuery("SELECT l FROM Loan l WHERE l.due = :due", Loan.class)
				.setParameter("id", due, TemporalType.DATE).getResultList();
	}

	@Override
	public Collection<Loan> findByCreationDate(Calendar created) {
		return super.getManager().createQuery("SELECT l FROM Loan l WHERE l.created = :created", Loan.class)
				.setParameter("created", created)
				.getResultList();
	}

	@Override
	public Collection<Loan> findLateFromUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Loan> findLate() {
		// TODO Auto-generated method stub
		return null;
	}

}
