package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import com.gitlab.labslab.bibliofile.model.LoginTracker;

public interface LoginTrackerDao {
	
	void save(LoginTracker obj);
	
	LoginTracker findById(long id);
	
	List<LoginTracker> list();
	
}
