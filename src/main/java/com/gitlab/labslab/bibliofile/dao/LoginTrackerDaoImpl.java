package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.LoginTracker;

@Repository
public class LoginTrackerDaoImpl implements LoginTrackerDao {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<LoginTracker> list() {
		return manager.createQuery("SELECT l FROM LoginTracker l", LoginTracker.class).getResultList();
	}

	@Override
	public LoginTracker findById(long id) {
		return manager.find(LoginTracker.class, id);
	}

	@Override
	@Transactional
	public void save(LoginTracker obj) {
		manager.persist(obj);

	}

}
