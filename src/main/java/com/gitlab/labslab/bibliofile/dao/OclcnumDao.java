package com.gitlab.labslab.bibliofile.dao;

import com.gitlab.labslab.bibliofile.model.Oclcnum;

public interface OclcnumDao extends Dao<Oclcnum> {
		Oclcnum	findByNum(String num);
}
