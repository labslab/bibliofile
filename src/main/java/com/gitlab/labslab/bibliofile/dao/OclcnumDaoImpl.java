package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Oclcnum;

@Repository
public class OclcnumDaoImpl extends AbstractDao<Oclcnum> implements OclcnumDao {

	@Override
	public Oclcnum findByNum(String num) {
		List<Oclcnum> nums = super.getManager().createQuery("SELECT o FROM Oclcnum o WHERE o.num  = :num", Oclcnum.class)
				.setParameter("num", num).getResultList();
		if (nums.isEmpty()) {
			return null;
		}
		return nums.get(0);
	}

}
