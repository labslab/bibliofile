package com.gitlab.labslab.bibliofile.dao;

import com.gitlab.labslab.bibliofile.model.PeerReview;

public interface PeerReviewDao extends Dao<PeerReview> {

	PeerReview findByName(String name);

}
