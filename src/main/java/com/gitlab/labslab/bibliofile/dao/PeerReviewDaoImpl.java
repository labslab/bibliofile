package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.PeerReview;

@Repository
public class PeerReviewDaoImpl extends AbstractDao<PeerReview> implements PeerReviewDao {

	@Override
	public PeerReview findByName(String name) {
		List<PeerReview> peers = super.getManager()
				.createQuery("SELECT p FROM PeerReview p WHERE UPPER(p.name) LIKE UPPER(:name)", PeerReview.class)
				.setParameter("name", "%" + name + "%").getResultList();
		if (peers.isEmpty()) {
			return null;
		}
		return peers.get(0);

	}

}
