package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import com.gitlab.labslab.bibliofile.model.Publisher;

public interface PublisherDao extends Dao<Publisher> {
	
	Publisher findByExactName(String name);

	List<Publisher> findByName(String name);
}
