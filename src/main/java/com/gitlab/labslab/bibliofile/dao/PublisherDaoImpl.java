package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Publisher;

@Repository
public class PublisherDaoImpl extends AbstractDao<Publisher> implements PublisherDao {

	@Override
	public Publisher findByExactName(String name) {
		List<Publisher> publishers = super.getManager()
				.createQuery("SELECT p FROM Publisher p WHERE p.name = :name", Publisher.class)
				.setParameter("name", name).getResultList();
		if (publishers.isEmpty()) {
			return null;
		}
		return publishers.get(0);
	}

	@Override
	public List<Publisher> findByName(String name) {
		return super.getManager().createQuery("SELECT p FROM Publisher p WHERE p.name LIKE '%:name%'", Publisher.class)
				.setParameter("name", name).getResultList();
	}

}
