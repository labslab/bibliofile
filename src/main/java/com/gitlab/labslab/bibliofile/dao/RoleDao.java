package com.gitlab.labslab.bibliofile.dao;

import com.gitlab.labslab.bibliofile.model.Role;

public interface RoleDao extends Dao<Role> {

	Role findByAuth(String auth);
}
