package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.Role;

@Repository
public class RoleDaoImpl extends AbstractDao<Role> implements RoleDao {

	@Override
	public Role findByAuth(String auth) {
		List<Role> roles= super.getManager().createQuery("select r from Role r where r.authority = :auth", Role.class)
				.setParameter("auth", auth)
				.getResultList();
		if(roles.isEmpty()) {
			return null;
		}
		return roles.get(0);
	}

}
