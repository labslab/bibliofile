/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.labslab.bibliofile.dao;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.gitlab.labslab.bibliofile.model.User;

/**
 *
 * @author boxx
 */
public interface UserDao extends Dao<User>{

	User loadUserByUsername(String username) throws UsernameNotFoundException ;
    
}
