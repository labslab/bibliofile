/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.labslab.bibliofile.dao;

import java.util.List;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import com.gitlab.labslab.bibliofile.model.User;

/**
 *
 * @author boxx
 */

@Repository
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		List<User> users = super.getManager().createQuery("select u from User u where username = :username", User.class)
				.setParameter("username", username).getResultList();
		if (users.isEmpty()) {
			throw new UsernameNotFoundException("User" + username + " not found. =(");
		} else {
			User user = users.get(0);
			return user;
		}
	}

}
