package com.gitlab.labslab.bibliofile.model;

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.labslab.bibliofile.utils.LocalDateTimeAttributeConverter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditableEntity {
	@JsonIgnore
	@CreatedDate
	@Convert(converter = LocalDateTimeAttributeConverter.class)
	@DateTimeFormat(pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime createdDate;
	@JsonIgnore
	@LastModifiedDate
	@Convert(converter = LocalDateTimeAttributeConverter.class)
	@DateTimeFormat(pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime lastModifiedDate;
	@JsonIgnore
	@LastModifiedBy
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(referencedColumnName = "id")
	private User lastModifiedBy;
	@JsonIgnore
	@CreatedBy
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(referencedColumnName = "id")
	private User createdBy;

	

	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
