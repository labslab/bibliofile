package com.gitlab.labslab.bibliofile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dto<T> {

	@JsonProperty
	private String stat;
	
	@JsonProperty(value = "list")
	private List<T> dtos;

	public List<T> getDtos() {
		return dtos;
	}

	public void setDtos(List<T> dtos) {
		this.dtos = dtos;
	}


	public String getStat() {
		return stat;
	}

	public void setStat(String stat) {
		this.stat = stat;
	}


}
