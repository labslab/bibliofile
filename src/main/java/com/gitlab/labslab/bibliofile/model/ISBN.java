package com.gitlab.labslab.bibliofile.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.gitlab.labslab.bibliofile.utils.LocalDateAttributeConverter;

@Entity
public class ISBN extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8907303199201360293L;

	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(targetEntity = Author.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(referencedColumnName = "id")
	private Collection<Author> author;
	private String lang;

	@Convert(converter = LocalDateAttributeConverter.class)
	@DateTimeFormat(pattern = "yyyy")
	private LocalDate year;

	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(targetEntity = Code.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(referencedColumnName = "id")
	private Collection<Code> code;

	private String ed;
	private String originalLang;

	public ISBN() {
		code = new HashSet<>();
		code.add(new Code());
		author = new HashSet<>();
		author.add(new Author());
	}

	public String getOriginalLang() {
		return originalLang;
	}

	public void setOriginalLang(String originalLang) {
		this.originalLang = originalLang;
	}

	public String getEd() {
		return ed;
	}

	public void setEd(String ed) {
		this.ed = ed;
	}

	public Collection<Code> getIsbn() {
		return code;
	}

	public void setIsbn(Collection<Code> isbn) {
		this.code = isbn;
	}

	public LocalDate getYear() {
		return year;
	}

	public Collection<Code> getCode() {
		return code;
	}

	public void setCode(Collection<Code> code) {
		this.code = code;
	}

	public void addCode(Code code) {
		this.code.add(code);
	}

	public void setYear(LocalDate year) {
		this.year = year;

	}

	public Collection<Author> getAuthor() {
		return author;
	}

	public void setAuthor(Collection<Author> author) {
		this.author = author;
	}

	public void addAuthor(Author author) {
		this.author.add(author);
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
