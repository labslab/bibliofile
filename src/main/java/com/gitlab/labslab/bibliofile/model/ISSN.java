package com.gitlab.labslab.bibliofile.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class ISSN extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5926761138331555210L;
	private String issnl;
	private String issn;
	@LazyCollection(LazyCollectionOption.FALSE)
	@Fetch(FetchMode.SELECT)
	@ManyToMany(targetEntity = PeerReview.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(referencedColumnName = "id")
	private Collection<PeerReview> peerReview = new HashSet<>();

	private String rawcoverage;

	public String getIssnl() {
		return issnl;
	}

	public Collection<PeerReview> getPeerReview() {
		return peerReview;
	}

	public void setPeerReview(Collection<PeerReview> peerReview) {
		this.peerReview = peerReview;
	}

	public void addPeerReview(PeerReview peerReview) {
		this.peerReview.add(peerReview);
	}

	public void setIssnl(String issnl) {
		this.issnl = issnl;
	}

	public String getRawcoverage() {
		return rawcoverage;
	}

	public void setRawcoverage(String rawcoverage) {
		this.rawcoverage = rawcoverage;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
