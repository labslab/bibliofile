package com.gitlab.labslab.bibliofile.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(value = { "lccn", "url", "rssurl" }, ignoreUnknown = true)
public class IsbnDto extends Dto<IsbnDto> {
	@JsonProperty("title")
	private String title;
	@JsonProperty("city")
	private String city;
	private List<String> publisher = new ArrayList<>();
	private List<String> author = new ArrayList<>();
	@JsonProperty("lang")
	private String lang;
	private LocalDate year;
	@JsonProperty("isbn")
	private List<String> code = new ArrayList<>();
	@JsonProperty("ed")
	private String ed;
	@JsonProperty("originalLang")
	private String originalLang;
	private List<String> form = new ArrayList<>();
	private List<String> oclcnum = new ArrayList<>();

	public List<String> getForm() {
		return form;
	}

	@JsonSetter(value = "form")
	public void addForm(Object form) {
		if (form instanceof List) {
			if (((List<?>) form).get(0) instanceof String) {
				this.form.addAll((List<String>) form);
			}

		} else if (form instanceof String) {
			this.form.add((String) form);
		}
	}

	@JsonSetter(value = "author")
	public void addAuthor(Object author) {
		if (author instanceof List) {
			if (((List<?>) author).get(0) instanceof String) {
				this.author.addAll((List<String>) author);
			}

		} else if (author instanceof String) {
			this.author.add((String) author);
		}
	}

	public void setForm(List<String> form) {
		this.form = form;
	}

	@JsonProperty(value = "publisher")
	public void addPublisher(Object publisher) {
		if (publisher instanceof List) {
			if (((List<?>) publisher).get(0) instanceof String) {
				this.publisher.addAll((List<String>) publisher);
			}

		} else if (publisher instanceof String) {
			this.publisher.add((String) publisher);
		}
	}

	public LocalDate getYear() {
		return year;
	}

	public void setYear(LocalDate year) {
		this.year = year;
	}

	@JsonSetter(value = "year")
	public void setYear(String year) {
		Integer yearInt = null;
		try {
			yearInt = Integer.valueOf(year);
		} catch (Exception e) {
			System.out.println("bad year =( " + year);
		}
		if (yearInt != null)
			this.year = LocalDate.ofYearDay(yearInt, 1);
	}

	public List<String> getPublisher() {
		return publisher;
	}

	public List<String> getAuthor() {
		return author;
	}

	public String getLang() {
		return lang;
	}

	public List<String> getCode() {
		return code;
	}

	public String getEd() {
		return ed;
	}

	public String getOriginalLang() {
		return originalLang;
	}

	public void setPublisher(List<String> publisher) {
		this.publisher = publisher;
	}

	public void setAuthor(List<String> author) {
		this.author = author;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public void setCode(List<String> code) {
		this.code = code;
	}

	public void setEd(String ed) {
		this.ed = ed;
	}

	public void setOriginalLang(String originalLang) {
		this.originalLang = originalLang;
	}

	public List<String> getOclcnum() {
		return oclcnum;
	}

	public void setOclcnum(List<String> oclcnum) {
		this.oclcnum = oclcnum;
	}

	public String getTitle() {
		return title;
	}

	public String getCity() {
		return city;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
