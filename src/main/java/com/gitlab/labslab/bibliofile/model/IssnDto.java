package com.gitlab.labslab.bibliofile.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(value = { "rel", "lccn", "url", "rssurl" }, ignoreUnknown = true)
public class IssnDto extends Dto<IssnDto> {
	private List<String> publisher = new ArrayList<>();
	private Long id;
	private String issnl;
	private String issn;
	@JsonProperty(value = "peerreview")
	private List<String> peerReview = new ArrayList<>();
	private List<String> oclcnum = new ArrayList<>();
	private String rawcoverage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIssnl() {
		return issnl;
	}

	public List<String> getPeerReview() {
		return peerReview;
	}

	public void setPeerReview(List<String> peerReview) {
		this.peerReview = peerReview;
	}

	public void setIssnl(String issnl) {
		this.issnl = issnl;
	}

	public String getRawcoverage() {
		return rawcoverage;
	}

	public void setRawcoverage(String rawcoverage) {
		this.rawcoverage = rawcoverage;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public List<String> getPublisher() {
		return publisher;
	}

	public List<String> getOclcnum() {
		return oclcnum;
	}

	public void setPublisher(List<String> publisher) {
		this.publisher = publisher;
	}

	public void setOclcnum(List<String> oclcnum) {
		this.oclcnum = oclcnum;
	}

}
