package com.gitlab.labslab.bibliofile.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Item extends AuditableEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4116104994152065175L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String title;
	private String city;

	@Enumerated(EnumType.STRING)
	private ItemState state;

	@ManyToMany(targetEntity = Publisher.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(referencedColumnName = "id")
	private Collection<Publisher> publisher;

	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(targetEntity = Oclcnum.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(referencedColumnName = "id")
	private Collection<Oclcnum> oclcnum;

	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(targetEntity = Form.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(referencedColumnName = "id")
	private Collection<Form> form;

	public Item() {
		publisher = new HashSet<>();
		oclcnum = new HashSet<>();
		form = new HashSet<>();
		publisher.add(new Publisher());
		oclcnum.add(new Oclcnum());
		form.add(new Form());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Collection<Publisher> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<Publisher> publisher) {
		this.publisher = publisher;
	}

	public void addForm(Form form) {
		this.form.add(form);
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Collection<Oclcnum> getOclcnum() {
		return oclcnum;
	}

	public void setOclcnum(List<Oclcnum> oclcnum) {
		this.oclcnum = oclcnum;
	}

	public Collection<Form> getForm() {
		return form;
	}

	public void setForm(List<Form> form) {
		this.form = form;
	}

	public ItemState getState() {
		return state;
	}

	public void setState(ItemState state) {
		this.state = state;
	}

	public void setPublisher(Collection<Publisher> publisher) {
		this.publisher = publisher;
	}

	public void setOclcnum(Collection<Oclcnum> oclcnum) {
		this.oclcnum = oclcnum;
	}

	public void setForm(Collection<Form> form) {
		this.form = form;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
