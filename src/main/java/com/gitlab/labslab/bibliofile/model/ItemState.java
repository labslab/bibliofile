package com.gitlab.labslab.bibliofile.model;

public enum ItemState {
	BORROWED("Emprestado"), 
	AVAILABLE("Disponivel"),
	UNAVAILABLE("Indisponível");

	private final String name;

	private ItemState(String name) {
		this.name = name;
	}
	
	public String getNmae() {
		return name;
	}

	public String getName() {
		return name;
	}
}
