package com.gitlab.labslab.bibliofile.model;

public enum ItemType {
	ISBN("Livro"),
	ISSN("Periódico");

	private final String name;

	private ItemType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
