package com.gitlab.labslab.bibliofile.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@SuppressWarnings("serial")
//@Entity
public class LibraryUser extends User {
	@OneToMany(mappedBy = "user", targetEntity = Loan.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<Loan> loans = new HashSet<>();

	@Transient
	private Integer dailyFee = 2;

	@Transient
	private Integer allowedLoansAmount = 5;

	public Collection<Loan> getLoans() {
		return loans;
	}

	public void setLoans(Collection<Loan> loans) {
		this.loans = loans;
	}

	public void addLoan(Loan loan) {
		this.loans.add(loan);
	}

	public boolean isAllowed() {
		return this.loans.size() < this.allowedLoansAmount && getDueFees() == BigDecimal.ZERO;
	}

	public BigDecimal getDueFees() {
		return this.loans.stream()
				.filter(l -> l.getDueDate().isBefore(LocalDate.now()))
				.map(l -> BigDecimal.valueOf(
						(LocalDate.now().getLong(ChronoField.EPOCH_DAY) - l.getDueDate().getLong(ChronoField.EPOCH_DAY))
								* dailyFee))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public Integer getAllowedLoansAmount() {
		return allowedLoansAmount;
	}

	public void setAllowedLoansAmount(Integer allowedLoansAmount) {
		this.allowedLoansAmount = allowedLoansAmount;
	}

	public Integer getDailyFee() {
		return dailyFee;
	}

	public void setDailyFee(Integer dailyFee) {
		this.dailyFee = dailyFee;
	}

}
