package com.gitlab.labslab.bibliofile.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.gitlab.labslab.bibliofile.utils.LocalDateAttributeConverter;

@Entity
public class Loan extends AuditableEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1318729479103349706L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(targetEntity = User.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(referencedColumnName = "id")
	private User user;

	@OneToOne(targetEntity = Item.class)
	@JoinColumn(referencedColumnName = "id")
	private Item item;

	@Convert(converter = LocalDateAttributeConverter.class)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate dueDate;

	@Enumerated(EnumType.STRING)
	private LoanState state;

	@Convert(converter = LocalDateAttributeConverter.class)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate returnDate;

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public Item getItem() {
		return item;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public LoanState getState() {
		return state;
	}

	public void setState(LoanState state) {
		this.state = state;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate due) {
		this.dueDate = due;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;

	}

	public LocalDate getReturnDate() {
		return this.returnDate;
	}

}
