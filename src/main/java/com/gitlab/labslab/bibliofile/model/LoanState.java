package com.gitlab.labslab.bibliofile.model;

public enum LoanState {

	ACTIVE("Ativo"), 
	OVERDUE("Atrasado"), 
	RETURNED("Devolvido");

	private final String name;

	private LoanState(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
