package com.gitlab.labslab.bibliofile.model;

public enum LoginAction {

	LOGIN, LOGOUT;
}
