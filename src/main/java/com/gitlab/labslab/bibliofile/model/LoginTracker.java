package com.gitlab.labslab.bibliofile.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@Embeddable
@Entity
public class LoginTracker extends AuditableEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3162878831991321159L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Enumerated(EnumType.STRING)
	private LoginAction action;

	public LoginTracker() {
	}

	public LoginTracker(LoginAction action) {
		this.action = action;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LoginAction getAction() {
		return action;
	}

	public void setAction(LoginAction action) {
		this.action = action;
	}

	
}
