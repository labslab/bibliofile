package com.gitlab.labslab.bibliofile.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Oclcnum implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2259346567037525090L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String num;

	public Oclcnum() {
	}

	public Oclcnum(String num) {
		this.num = num;
	}

	public Long getId() {
		return id;
	}

	public String getNum() {
		return num;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
