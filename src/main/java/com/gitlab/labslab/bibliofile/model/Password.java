package com.gitlab.labslab.bibliofile.model;

public class Password {

	String original;
	String confirmation;

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	public boolean isValid() {
		
		return original.equals(confirmation);
	}

}
