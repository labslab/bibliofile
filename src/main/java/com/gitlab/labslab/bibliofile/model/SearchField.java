package com.gitlab.labslab.bibliofile.model;

public enum SearchField {

	AUTHOR("Autor"), 
	TITLE("Título"), 
	PUBLISHER("Editora"),
	CODE("ISBN/ISSN");
	
	private final String name;

	private SearchField(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}