package com.gitlab.labslab.bibliofile.model;

import java.util.ArrayList;
import java.util.List;

public class SearchForm {
	private String searchString;

	private List<SearchField> field = new ArrayList<>();

	
	public void addField(SearchField field) {
		this.field.add(field);
	}

	public List<SearchField> getField() {
		return field;
	}

	public void setField(List<SearchField> field) {
		this.field = field;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

}
