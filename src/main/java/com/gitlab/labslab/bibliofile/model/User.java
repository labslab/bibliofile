/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.labslab.bibliofile.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author boxx
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable, UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 883645228662486793L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@JsonIgnore
	@Column(nullable = false)
	private String password;

	@Column(unique = true, nullable = false)
	private String username;

	@JsonIgnore
	@JoinColumn(referencedColumnName = "id")
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE }, targetEntity = Role.class, fetch = FetchType.EAGER)
	private Collection<Role> roles = new HashSet<>();
	
	@JsonIgnore
	@OneToMany(targetEntity = LoginTracker.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH })
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(referencedColumnName = "id")
	private Collection<LoginTracker> loginTrack = new HashSet<>();
	
	@OneToMany( targetEntity = Loan.class, cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@Fetch(FetchMode.SELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(referencedColumnName = "id")
	private Collection<Loan> loans = new HashSet<>();

	@Transient
	private Integer dailyFee = 2;

	@Transient
	private Integer allowedLoansAmount = 5;

	public Collection<Loan> getLoans() {
		return loans;
	}

	public void setLoans(Collection<Loan> loans) {
		this.loans = loans;
	}

	public void addLoan(Loan loan) {
		this.loans.add(loan);
	}

	public boolean isAllowed() {
		return this.loans.size() < this.allowedLoansAmount && getDueFees() == BigDecimal.ZERO;
	}

	@JsonProperty
	@ReadOnlyProperty
	public BigDecimal getDueFees() {
		BigDecimal bd = this.loans.stream().filter(l -> l.getDueDate().isBefore(LocalDate.now()))
				.map(l -> BigDecimal.valueOf(
						(LocalDate.now().getLong(ChronoField.EPOCH_DAY) - l.getDueDate().getLong(ChronoField.EPOCH_DAY))
								* dailyFee))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		return bd;
	}

	public Integer getAllowedLoansAmount() {
		return allowedLoansAmount;
	}

	public void setAllowedLoansAmount(Integer allowedLoansAmount) {
		this.allowedLoansAmount = allowedLoansAmount;
	}

	public Integer getDailyFee() {
		return dailyFee;
	}

	public void setDailyFee(Integer dailyFee) {
		this.dailyFee = dailyFee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void addLoginTracker(LoginTracker loginTrack) {
		if (this.loginTrack == null) {
			this.loginTrack = new HashSet<>();
		}

		this.loginTrack.add(loginTrack);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof User)) {
			return false;
		}
		User other = (User) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.gitlab.labslab.bibliofile.config.model.Usuario[ id=" + id + " ]";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Collection<LoginTracker> getLoginTrack() {
		return loginTrack;
	}

	public void setLoginTrack(Collection<LoginTracker> loginTrack) {
		if (loginTrack == null) {
			this.loginTrack = new HashSet<>();
		}
		this.loginTrack = loginTrack;
	}

}
