package com.gitlab.labslab.bibliofile.model;

import java.util.Collection;

public class UserDto {

	private String username;
	private Collection<Role> roles;
	private Password password;
	public UserDto() {
	}

	public UserDto(String username, Password password, Collection<Role> roles) {
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public UserDto(String username, Password password) {
		this.username = username;
		this.password = password;
		this.roles = null;
	}

	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	public void addRole(Role role) {
		this.roles.add(role);
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
