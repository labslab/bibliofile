package com.gitlab.labslab.bibliofile.utils;

import java.beans.PropertyEditorSupport;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.stereotype.Component;

@Converter(autoApply = true)
@Component
public class LocalDateAttributeConverter extends PropertyEditorSupport implements AttributeConverter<LocalDate, Date> {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(LocalDate.parse(text, DateTimeFormatter.ofPattern("dd-MM-yyyy")));
	}

	@Override
	public String getAsText() throws IllegalArgumentException {
		return ((LocalDate) getValue()).toString();
	}

	@Override
	public Date convertToDatabaseColumn(LocalDate locDate) {
		return (locDate == null ? null : Date.valueOf(locDate));
	}

	@Override
	public LocalDate convertToEntityAttribute(Date sqlDate) {
		return (sqlDate == null ? null : sqlDate.toLocalDate());
	}

	
}
