package com.gitlab.labslab.bibliofile.utils;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.stereotype.Component;

@Converter(autoApply = true)
@Component
public class LocalDateTimeAttributeConverter extends PropertyEditorSupport
		implements AttributeConverter<LocalDateTime, Timestamp> {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text != null && !text.isEmpty())
			setValue(LocalDateTime.parse(text, DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss")));
	}

	@Override
	public String getAsText() throws IllegalArgumentException {
		return ((LocalDateTime) getValue()).toString();
	}

	@Override
	public Timestamp convertToDatabaseColumn(LocalDateTime locDateTime) {
		return (locDateTime == null ? null : Timestamp.valueOf(locDateTime));
	}

	@Override
	public LocalDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
		return (sqlTimestamp == null ? null : sqlTimestamp.toLocalDateTime());
	}
}
