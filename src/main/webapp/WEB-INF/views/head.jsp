<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<!DOCTYPE html>
<html>
<head>


<c:url value="${request.contextPath}/resources/css" var="cssPath" />
<c:url value="${request.contextPath}/resources/js" var="jsPath" />
<link rel="stylesheet" href="${cssPath}/bootstrap.min.css">
<link rel="stylesheet" href="${cssPath}/bootstrap-theme.min.css">

<script src="${jsPath}/jquery-3.1.0.min.js"></script>
<script src="${jsPath }/bootstrap.min.js"></script>
<title>Bibliofile - Sua biblioteca do futuro.</title>
<style type="text/css">
body {
	padding: 60px 60px;
}
</style>

</head>
<body>
	<nav  role="navigation" class="navbar navbar-inverse navbar-fixed-top">
<!-- 	<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top"> -->
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${s:mvcUrl('HC#home').build() }">Biblioteca</a>

			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<!-- 				<ul class="nav navbar-nav navbar-left"> -->
				<%-- 					<li><a href="${s:mvcUrl('IC#list').build() }"> Listar --%>
				<!-- 							Itens </a></li> -->
				<!-- 				</ul> -->
				<ul class="nav navbar-nav navbar-left">
					<li><a href="${s:mvcUrl('SC#main').build() }"> Busca </a></li>
				</ul>
				<security:authorize access="hasRole('ROLE_LIBRARIAN')">

					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('RIC#main').build() }"> Cadastrar
								Itens </a></li>
					</ul>
				</security:authorize>
				<security:authorize access="!isAuthenticated()">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('LC#login').build() }"> Login </a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('UC#main').build() }"> Cadastrar
						</a></li>
					</ul>
				</security:authorize>
				<security:authorize access="isAuthenticated()">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('LC#logout').build() }"> <security:authentication
									property="principal" var="usuario" /> Logout

						</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('LMC#loans').build() }"> <security:authentication
									property="principal" var="usuario" /> Usuário:
								${usuario.username }
						</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('LMC#loans').build() }"> Empréstimos

						</a></li>
					</ul>
				</security:authorize>
			</div>
		</div>
	</nav>
	<div class="container" style="float: center; text-align: center">
		<div class="col-xs-11">
			<div class="row">
				<c:import url="msgs.jsp" />
			</div>
		</div>

	</div>