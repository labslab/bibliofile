<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<c:import url="head.jsp" />

<br />
<div class="container col-xs-12"
	style="displpay: inline-block; text-align: center">
	<div class="table-responsive">
		<table style="text-align: center"
			class="table table-striped table-bordered table-hover table-responsive">
			<thead>
				<tr>
					<th style="text-align: center">ID</th>
					<th style="text-align: center">T�tulo</th>
					<th style="text-align: center">Editora</th>
					<th style="text-align: center">Autor</th>
					<th style="text-align: center">Cidade</th>
					<th style="text-align: center">ISBN/ISSN</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${items }" var="item" varStatus="i">
					<tr>
						<td>${item.id}</td>
						<td><security:authorize access="hasRole('ROLE_LIBRARIAN')">
								<a href="${s:mvcUrl('LMC#newLoan').arg(0, item.id).build()}">
									${item.title }</a>
							</security:authorize> <security:authorize access="!hasRole('ROLE_LIBRARIAN')">
								<a href="${s:mvcUrl('IC#details').arg(0, item.id).build()}">
									${item.title }</a>
							</security:authorize></td>
						<td><c:forEach items="${item.publisher }" var="pub"
								varStatus="i">
						${pub.name}${i.last ? '':',' }
						</c:forEach></td>
						<td><c:forEach items="${item.author }" var="auth"
								varStatus="i">
								${auth.name }${i.last ? '':',' }
							</c:forEach></td>
						<td>${item.city }</td>
						<td><c:forEach items="${item.code }" var="codex"
								varStatus="i">
								${codex.code }${i.last ? '':',' }
							</c:forEach></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
</body>
</html>