<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:import url="head.jsp" />

<br />

<div class="container col-xs-9"
	style="displpay: inline-block; text-align: center">
	<form:form action="${s:mvcUrl('LMC#createLoan').build() }" method="get"
		cssClass="form-group">

		<div class="table-responsive">
			<table style="text-align: center"
				class="table table-striped table-bordered table-hover table-responsive">
				<thead valign="middle">
					<tr>
						<th style="text-align: center">ID</th>
						<th style="text-align: center">T�tulo</th>
						<th style="text-align: center">Editora</th>
						<th style="text-align: center">Autor</th>
						<th style="text-align: center">Cidade</th>
						<th style="text-align: center">ISBN/ISSN</th>
						<!-- 					<th style="text-align: center">Tipo</th> -->
						<th style="text-align: center">Condi��o</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${item.id}</td>
						<td>${item.title }</td>
						<td><c:forEach items="${item.publisher }" var="pub"
								varStatus="i">
						${pub.name}${i.isLast() ? '':',' }
						</c:forEach></td>
						<td><c:forEach items="${item.author }" var="auth"
								varStatus="i">
								${auth.name }${i.isLast() ? '':',' }
							</c:forEach></td>
						<td>${item.city }</td>
						<td><c:forEach items="${item.code }" var="codex"
								varStatus="i">
								${codex.code }${i.isLast() ? '':',' }
							</c:forEach></td>
						<td>${item.state.name }</td>
					</tr>
				</tbody>


			</table>
			<div class="col-xs-6">
				<div class="row">
					<label>Username usu�rio</label><input type="text" name="username"
						class="form-control">
				</div>
				<div class="row">
					<input type="hidden" name="idItem" value="${item.id }"> <input
						type="submit" class="bnt" value="efetuar empr�stimo">
				</div>
			</div>
		</div>

	</form:form>
</div>

</body>
</html>