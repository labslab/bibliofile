<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<c:import url="head.jsp" />

<br />
<div class="container col-xs-12"
	style="displpay: inline-block; text-align: center">
	<div class="row">
		<div class="col-xs-2">
			<table
				style="text-align: center; float: left; padding-left: 0px;"
				class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th style="text-align: center">Multas</th>
						<th style="text-align: center">Habilitado</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${user.dueFees}</td>
						<td>${user.allowed}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- 	<div class="row" style="clear: left; text-align: left"> -->
		<!-- 		<div class="row"> -->
		<%-- 			<label>Multas:</label> ${user.getDueFees()} --%>
		<!-- 		</div> -->
		<!-- 		<div class="row"> -->
		<%-- 			<label>Autorizado:</label> ${user.isAllowed() } --%>
		<!-- 		</div> -->
		<!-- 	</div> -->
		<div class="col-xs-10">
			<div class="table-responsive">
				<table style="text-align: center;  float: right; padding-right: 0px"
					class="table table-striped table-bordered table-hover table-compact">
					<thead>
						<tr>
							<th style="text-align: center">ID</th>
							<th style="text-align: center">T�tulo</th>
							<th style="text-align: center">Autor</th>
							<th style="text-align: center">Efetuado em</th>
							<th style="text-align: center">Devolu��o</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${loans }" var="loan" varStatus="i">
							<tr>
								<td>${loan.id}</td>
								<td><form:form cssStyle="float: left"
										action="${s:mvcUrl('LMC#renewLoan').build() }" method="get">
										<input type="hidden" name="idLoan" value="${loan.id }">
										<input type="submit" class="btn btn-submit" value="renovar">
									</form:form> ${loan.item.title }</td>
								<td><c:forEach items="${loan.item.author }" var="auth"
										varStatus="i">
								${auth.name } ${i.isLast() ? '':',' }</c:forEach></td>
								<c:set var="cleanedDateTime"
									value="${fn:replace(loan.createdDate, 'T', ' ')}" />
								<fmt:parseDate value="${ cleanedDateTime }"
									pattern="yyyy-MM-dd HH:mm" var="parsedDateTime" type="both" />
								<td><fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss"
										value="${ parsedDateTime }" /></td>
								<td>${loan.dueDate }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</body>
</html>