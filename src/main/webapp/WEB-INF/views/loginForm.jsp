<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<c:import url="head.jsp" />

<div class="container"
	style="width: 500px; displpay: inline-block; text-align: center">
	<form:form servletRelativeAction="/login" modelAttribute="user"
		method="post" cssStyle="form-group">
		<legend class="text-info">Login</legend>

		<div class="container col-xs-8">

			<div class="row">
				<label class="form-label text-primary" style="float: left">
					Username:</label>
				<div style="float: right">
					<form:input title="username" id="username" path="username"
						cssClass="form-control" align="right" cssErrorClass="form-error" />
				</div>
			</div>
			<br/>
			<div class="row">
				<label class="form-label text-primary">Senha:</label>
				<div style="float: right">
					<form:password title="Senha" id="password" path="password"
						cssClass="form-control" cssErrorClass="form-error" />
				</div>
			</div>


		</div>
		
		<input style="clear: right" type="submit" value="login" class="btn clearfix">

	</form:form>

</div>

</body>
</html>