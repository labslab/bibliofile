<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<c:import url="head.jsp" />

<div class="container col-xs-15"
	style="width: 450px; displpay: inline-block; text-align: center">
	<form:form servletRelativeAction="${s:mvcUrl('UC#register').build() }"
		modelAttribute="user" method="post" cssStyle="form-group">
		<legend class="text-info">Cadastro</legend>
		<div class="container col-xs-12">
			<div class="container col-xs-11">
				<div class="row">
					<label class="form-label text-primary" style="float: left">Username:
					</label>
					<div style="float: right">
						<form:input id="username" path="username" cssClass="form-control"
							cssErrorClass="form-error" />
					</div>
				</div>
				<br />
				<div class="row">
					<label class="form-label text-primary" style="float: left">Senha:
					</label>
					<div style="float: right">
						<form:password path="password.original" cssClass="form-control"
							maxlength="20" cssErrorClass="form-error" />
					</div>
				</div>
				<br />
				<div class="row">
					<label class="form-label text-primary" style="float: left">Confirmar
						senha: </label>
					<div style="float: right">
						<form:password path="password.confirmation"
							cssClass="form-control" maxlength="20" cssErrorClass="form-error" />
					</div>
				</div>
				<br/>
				<div class="row clearfix" >
					<input type="submit" class="btn" value="Cadastrar">
				</div>
			</div>
		</div>
	</form:form>

</div>

</body>
</html>
