<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:import url="head.jsp" />
<div class="container col-xs-13"
	style="displpay: inline-block; text-align: center">
	<form:form
		servletRelativeAction="${s:mvcUrl('ISBNC#registrar').build() }"
		modelAttribute="item" method="post" cssClass="col-xs-13 form-group">
		<legend>Registrar ISBN</legend>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">
				<label class="control-label">ISBN: </label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="code[0].code" title="ISBN" value="${code[0].code}"
					cssClass="form-control" />
			</div>
		</div>
		<c:forEach items="${code}" var="isbnvar" varStatus="i" begin="1">
			<div class="row">
				<div class="col-xs-3" style="clear: left; text-align: right">
					<label class="control-label">ISBN: </label>
				</div>
				<div class="col-xs-9" style="clear: right">
					<form:input path="code[${i.index }].code" title="ISBN"
						value="${isbnvar.code}" cssClass="form-control" />
				</div>
			</div>
		</c:forEach>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">

				<label class="control-label"> Autor:</label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="author[0].name" title="Autor"
					value="${author[0].name}" cssClass="form-control " />
			</div>
		</div>
		<c:forEach items="${author}" var="auth" varStatus="i" begin="1">
			<div class="row">
				<div class="col-xs-3" style="clear: left; text-align: right">
					<label class="control-label"> Autor:</label>
				</div>
				<div class="col-xs-9" style="clear: right">
					<form:input path="author[${i.index }].name" title="Autor"
						value="${auth.name}" cssClass="form-control" />
				</div>

			</div>
		</c:forEach>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">
				<label class="control-label">T�tulo: </label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="title" title="T�tulo" value="${title }"
					cssClass="form-control" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">
				<label class="control-label">Editora: </label>
			</div>
			<div class="col-xs-9" style="cleart: right">
				<form:input path="publisher[0].name" title="Editora"
					value="${publisher[0].name}" cssClass="form-control" />
			</div>
		</div>
		<c:forEach items="${publisher}" var="pub" varStatus="i" begin="1">
			<div class="row">
				<div class="col-xs-3" style="clear: left; text-align: right">
					<label class="control-label">Editora: </label>
				</div>
				<div class="col-xs-9" style="clear: right">
					<form:input path="publisher[${i.index }].name" title="Editora"
						value="${pub.name}" cssClass="form-control" />
				</div>
			</div>
		</c:forEach>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">

				<label class="control-label"> L�ngua:</label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="lang" title="L�ngua" value="${lang}"
					cssClass="form-control " />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">

				<label class="control-label"> L�ngua Original:</label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="originalLang" title="L�ngua Original"
					value="${originalLang}" cssClass="form-control " />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">

				<label class="control-label"> Edi��o:</label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="ed" title="Edi��o" value="${ed}"
					cssClass="form-control " />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">

				<label class="control-label"> Ano:</label>
			</div>

			<div class="col-xs-9" style="clear: right">
					<form:input path="year" title="L�ngua Original"
						value="${year}" cssClass="form-control " />
				
			</div>

		</div>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">
				<label class="control-label"> Formato: </label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="form[0].name" title="Formato"
					value="${form[0].name}" cssClass="form-control" />
			</div>
		</div>
		<c:forEach items="${form}" var="formvar" varStatus="i" begin="1">
			<div class="row">
				<div class="col-xs-3" style="clear: left; text-align: right">
					<label class="control-label"> Formato: </label>
				</div>
				<div class="col-xs-9" style="clear: right">
					<form:input path="form[${i.index }].name" title="Formato"
						value="${formvar.name}" cssClass="form-control" />
				</div>
			</div>
		</c:forEach>

		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">
				<label class="control-label"> Cidade:</label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="city" title="Cidade" value="${city }"
					cssClass="form-control" />
			</div>

		</div>
		<div class="row">
			<div class="col-xs-3" style="clear: left; text-align: right">
				<label class="control-label"> Oclcnum: </label>
			</div>
			<div class="col-xs-9" style="clear: right">
				<form:input path="oclcnum[0].num" title="Oclcnum"
					value="${oclcnum[0].num}" cssClass="form-control" />
			</div>

		</div>
		<c:forEach items="${oclcnum}" var="ocl" begin="1" varStatus="i">
			<div class="row">
				<div class="col-xs-3" style="clear: left; text-align: right">
					<label class="control-label"> Oclcnum: </label>
				</div>
				<div class="col-xs-9" style="clear: right">
					<form:input path="oclcnum[${i.index }].num" title="Oclcnum"
						value="${ocl.num}" cssClass="form-control" />
				</div>

			</div>
		</c:forEach>

		<br />
		<div class="col-xs-3" style="clear: left; text-align: right">
			<input type="submit" class="btn" value="Registrar">
		</div>

	</form:form>

</div>

</body>
</html>
