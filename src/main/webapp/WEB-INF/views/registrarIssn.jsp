<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<c:import url="head.jsp" />
<div class="container" align="left">
	<form:form servletRelativeAction="${s:mvcUrl('ISSNC#registrar').build() }" modelAttribute="item"
		method="post" cssStyle="form-control">
		<s:message></s:message>
		<legend>
			<form:input path="issn" title="ISSN" value="${issn }" cssClass="form-control" /> 
			<form:input path="issnl" title="ISSNL"  value="${issnl }" cssClass="form-control" />


			<input type="submit" class="button">

		</legend>

	</form:form>

</div>

</body>
</html>