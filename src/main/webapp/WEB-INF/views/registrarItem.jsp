<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<c:import url="head.jsp" />
<div class="clearfix">
	<br />
</div>
<div class="container"
	style="displpay: inline-block; text-align: center;">
	<div style="float: center" class="col-xs-11 form-group">
		<div class="row">
			<form>
				<div class="col-xs-2" style="clear: left; text-align: right">
					<label for="issn">ISSN:</label>
				</div>
				<div class="col-xs-7">
					<input id="issn" type="text" title="issn" class="form-control"
						name="issn">
				</div>
				<div class="col-xs-2" style="clear: right">
					<input type="submit" formmethod="get" value="buscar" title="buscar"
						class="btn"
						formaction="${s:mvcUrl('ISSNC#fromJson').arg(0, issn).build()}">
				</div>
			</form>
		</div>
		<br />
		<div class="row">
			<form>
				<div class="col-xs-2" style="clear: left; text-align: right">
					<label for="issn">ISBN:</label>
				</div>
				<div class="col-xs-7" style="float: center">
					<input id="isbn" title="isbn" name="isbn" type="text"
						class="form-control">
				</div>
				<div class="col-xs-2" style="clear: right">
					<input type="submit" value="buscar" formmethod="get" title="buscar"
						class="btn"
						formaction="${s:mvcUrl('ISBNC#fromJson').arg(0, isbn).build()}">
				</div>
			</form>
		</div>
	</div>

</div>

</body>
</html>
