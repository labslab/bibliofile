<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<c:import url="head.jsp" />
<div class="clearfix">
	<br />
</div>
<br />
<div class="container"
	style="displpay: inline-block; text-align: center;">
	<form:form action="${s:mvcUrl('SC#search').build() }"
		modelAttribute="searchForm" method="post">
		<div style="float: center" class="col-xs-14 form-group">

			<div class="row">

				<div class="col-xs-2" style="clear: left; text-align: right">
					<label for="issn">Busca: </label>
				</div>
				<div class="col-xs-7" style="float: center">
					<form:input id="search" title="search text" path="searchString"
						cssClass="form-control" />
				</div>

				<div class="col-xs-1" style="clear: right">
					<input type="submit" value="buscar" title="buscar" class="btn">
				</div>



				<div class="col-xs-2" style="text-align: left; cleart: right">
					<form:checkboxes items="${fields }" path="field" element="div"
						cssStyle="text-align: left" cssClass="row" />
				</div>
			</div>
		</div>

	</form:form>
</div>

</body>
</html>
