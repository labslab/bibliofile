<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<c:import url="head.jsp" />

<br />
<div class="container">
	<div class="col-xs-9">
		<div class="container"
			style="displpay: inline-block; text-align: center;">
			<table
				class="table table-striped table-bordered table-hover table-responsive">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nome</th>
						<th>Auth</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users}" var="user">
						<tr>
							<td>${user.id}</td>
							<td>${user.username }</td>
							<td><c:forEach items="${user.roles }" var="role"
									varStatus="i">
							${role.authority}${i.last ? '':',' }
								</c:forEach></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>